const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    templateName: { type: String, required: true },
    subject: { type: String },
    body: { type: String },
},{
  timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('EmailTemplate', schema);
