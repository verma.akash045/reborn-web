﻿const express = require('express');
const router = express.Router();
const session = require('express-session')
const emailTemplate = require('./emailTemplate.service');

// routes
// Get
router.get('/', getTemplate);
router.get('/forgotPassword', getForgotPasswordTemplate);
router.get('/appointmentTemplate', getAppointmentTemplate);
router.get('/welcomeTemplate', getWelcomeTemplate);
// POST
router.post('/create', createTemplate);
router.post('/update', updateTemplate);

module.exports = router;

// Function for get forgot password template
function getForgotPasswordTemplate(req, res, next) {
    if(req.session.token){
        emailTemplate.getTemplate('forgotPassword')
        .then(emailTemplate => emailTemplate ? res.render('emailTemplatesForms/forgotPassword', {page:'Forgot Password Template', emailTemplate:emailTemplate,  error:""}) : res.render('emailTemplatesForms/forgotPassword', {page:'Forgot Password Template', emailTemplate:"", error:""} ) )
        .catch(err => res.render('emailTemplatesForms/forgotPassword', {page:'Forgot Password Template', emailTemplate:"", error:err}));
    }else{
        res.redirect('/adminLogin')
    }
}

// Function for get Appointment template
function getAppointmentTemplate(req, res, next) {
    if(req.session.token){
        emailTemplate.getTemplate('createAppointment')
        .then(emailTemplate => emailTemplate ? res.render('emailTemplatesForms/createAppointment', {page:'Appointment Template', emailTemplate:emailTemplate,  error:""}) : res.render('emailTemplatesForms/createAppointment', {page:'Appointment Template', emailTemplate:"", error:""} ) )
        .catch(err => res.render('emailTemplatesForms/createAppointment', {page:'Appointment Template', emailTemplate:"", error:err}));
    }else{
        res.redirect('/adminLogin')
    }
}

// Function for get welcome aborad template
function getWelcomeTemplate(req, res, next) {
    if(req.session.token){
        emailTemplate.getTemplate('welcomeAboard')
        .then(emailTemplate => emailTemplate ? res.render('emailTemplatesForms/welcomeAboard', {page:'Welcome User Template', emailTemplate:emailTemplate,  error:""}) : res.render('emailTemplatesForms/welcomeAboard', {page:'Welcome User Template', emailTemplate:"", error:""} ) )
        .catch(err => res.render('emailTemplatesForms/welcomeAboard', {page:'Welcome User Template', emailTemplate:"", error:err}));
    }else{
        res.redirect('adminLogin')
    }
}

// Function to create template
function createTemplate(req, res, next) {
    emailTemplate.createTemplate(req.body)
    .then(emailTemplate => emailTemplate ? res.json(emailTemplate) : res.status(400).json({ status: "error", message: "Error while creating template" }) )
    .catch(err => next(err));
}

// Function to create template
function updateTemplate(req, res, next) {
    emailTemplate.updateTemplate(req.body, req.query.templateId)
    .then(emailTemplate => emailTemplate ? res.json(emailTemplate) : res.status(400).json({ status: "error", message: "Error while updating template" }) )
    .catch(err => next(err));
}

function getTemplate(req, res, next) {
    if(req.session.token){
        res.render('emailTemplatesForms/emailTemplate', {page : 'Communucation'})
    }else{
        res.redirect('/adminLogin')
    }
}
