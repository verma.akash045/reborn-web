﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const EmailTemplate = db.EmailTemplate;

module.exports = {
    getTemplate,
    createTemplate,
    updateTemplate
};


/**
 * API for get template
 */
async function getTemplate(templateName) {
  let emailTemplate = await EmailTemplate.findOne({templateName:templateName})
  if(emailTemplate && emailTemplate.templateName){
    return emailTemplate
  }

}

/**
 * API for create templates and save into our DB
 */
async function createTemplate(bodyParams) {

  const emailTemplate = new EmailTemplate(bodyParams)

  await emailTemplate.save()

  return {status:200, message:"Template Created successfully"}
}

/**
 * API for update templates and save into our DB
 */
async function updateTemplate(bodyParams, templateID) {
  
  let emailTemplate = await EmailTemplate.findById(templateID)

  Object.assign(emailTemplate, bodyParams);
  await emailTemplate.save()

  return {status:200, message:"Template Updated successfully"}
}
