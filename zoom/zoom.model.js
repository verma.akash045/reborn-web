const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    access_token: { type: String, required: true },
    expires_in: { type: String, required: true },
    refresh_token: { type: String, required: true },
},{
  timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Zoom', schema);
