﻿const express = require('express');
const router = express.Router();
const diseaseConditionService = require('./diseaseCondition.service');

// routes
router.get('/', getAll);
router.post('/createDisease', createDisease);
router.get('/:id', getById);

module.exports = router;
// Function is for get all disease
function getAll(req, res, next) {
    diseaseConditionService.getAll()
        .then(diseaseConditions => res.json(diseaseConditions))
        .catch(err => next(err));
}
// Function is for create disease
function createDisease(req, res, next) {
    diseaseConditionService.create(req.body)
        .then(diseaseConditions => diseaseConditions ? res.json(diseaseConditions) : res.status(400).json({ status: "error", message: 'Error while creating disease condition' }))
        .catch(err => next(err));
}
// Function is for get disease by ID
function getById(req, res, next) {
    diseaseConditionService.getById(req.params.id)
        .then(diseaseConditions => diseaseConditions ? res.json(diseaseConditions) : res.status(404).json({ status: "error", message: 'Disease condition not found' }))
        .catch(err => next(err));
}
