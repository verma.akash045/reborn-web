﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const DiseaseCondition = db.DiseaseCondition;

module.exports = {
    getAll,
    create,
    getById
};

/**
 * API for get all disease
 */
async function getAll() {
    return await DiseaseCondition.find({ $or: [ { type: "None" }, { type: "Multi-Choice" }, {title: "Other"} ] });
}

/**
 * API for create disease
 */
async function create(diseaseConditionParam) {

  if (!diseaseConditionParam.title) {
    throw 'Title is required'
  }

  const diseaseCondition = new DiseaseCondition(diseaseConditionParam);

  // save goal
  await diseaseCondition.save();
  if (diseaseCondition && diseaseCondition.id){
    return {status:"success", message:"Disease condition created successfully", diseaseCondition:diseaseCondition}
  }
}

/**
 * API for get disease by ID
 */
async function getById(id) {
    let diseaseConditionID = mongoose.Types.ObjectId(id);
    return await DiseaseCondition.findById(diseaseConditionID);
}
