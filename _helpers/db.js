const config = require('config.json');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model'),
    DiseaseCondition: require('../diseaseConditions/diseaseCondition.model'),
    Goal: require('../goals/goal.model'),
    Drchrono: require('../drchronos/drchrono.model'),
    Doctor: require('../doctors/doctor.model'),
    Appointment: require('../appointments/appointment.model'),
    Zoom: require('../zoom/zoom.model'),
    Clinic: require('../clinics/clinic.model'),
    EmailTemplate: require('../emailTemplates/emailTemplate.model'),
    Feedback: require('../feedbacks/feedback.model'),
    Medicine: require('../medicines/medicine.model')
};
