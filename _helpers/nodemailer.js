var nodemailer = require('nodemailer')
const config = require('config.json')
async function mailer(email, link) {
    console.log(' in nodemailer params ->>', email, link, ' -->> end <---');
    var transporter = nodemailer.createTransport({
        service: 'Mail',
        host: config.senderHost,
        port: config.senderPort,
        secure: false,
        tls: {
            rejectUnauthorized:false
        },
        auth: {
            user: config.senderEmail,
            pass: config.senderPassword
        }
    });

    let mailOptions = {
        from: config.senderEmail,
        to: email,
        subject: 'Reset your account password',
        html: '<p>Click <a href="' + link + '">here</a> to reset your password</p>'
    };

    console.log('created');
    transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
            console.log(err, ' this is error in nodemmailer');
        }
        if (info) {
            console.log(info, " this is info in nodemailer.")
        }
    })
}

module.exports = mailer;
