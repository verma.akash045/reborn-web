const mailgun = require("mailgun-js");
const config = require('config.json')
const mg = mailgun({apiKey: config.mailgun_key, domain: config.mailgun_domain});

async function sendMail(email, subject, body) {

  const data = {
  	from: config.senderName+' <'+config.senderEmail+'>',
  	to: email,
  	subject: subject,
  	html: body
  };
  mg.messages().send(data, function (error, body) {
    console.log(data)
  	console.log(body);
  });

}

module.exports = sendMail;
