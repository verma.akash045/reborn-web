const expressJwt = require('express-jwt');
const config = require('config.json');
const userService = require('../users/user.service');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/login',
            '/adminLogin',
            '/adminLogout',
            '/signUp',
            '/updatePassword',
            '/signUpEmail',
            '/forgotPassword',
            '/setNewPasswordWithToken',
            '/signUpPhoneNumber',
            '/verifyPhoneNumber',
            '/drchrono/generate',
            '/drchrono',
            '/drchrono/iframe',
            '/reset',
            '/doctors/getDoctors',
            '/doctors/addDoctors',
            '/doctors/deleteDoctors',
            '/doctors/updateDoctors',
            '/clinics',
            '/clinics/create',
            '/clinics/update',
            '/clinics/delete',
            '/clinics/search',
            '/communication/forgotPassword',
            '/communication/welcomeTemplate',
            '/communication/appointmentTemplate',
            '/communication/update',
            '/communication/create',
            '/members',
            '/members/delete',
            '/members/create',
            '/members/update',
            { url: /^\/members\/details\/.*/, methods: ['GET'] },
            { url: /^\/clinics\/details\/.*/, methods: ['GET'] },
            { url: /^\/doctors\/details\/.*/, methods: ['GET'] },
            '/doctors/updateSessions',
            '/doctors/addSessions',
            '/doctors/updateTemplate',
            '/medicines',
            '/medicines/create',
            '/medicines/update',
            '/medicines/delete',
            '/communication'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};
