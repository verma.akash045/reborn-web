const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    allow_overlapping: { type: Boolean },
    appt_is_break: { type: Boolean },
    base_recurring_appointment: { type: String },
    billing_status: { type: String },
    billing_provider: { type: String },
    billing_notes: { type: Array },
    clinical_note: { type: String },
    cloned_from: { type: String },
    color: { type: String },
    custom_fields: { type: Array },
    custom_vitals: { type: Array },
    deleted_flag: { type: Boolean },
    doctorID: { type: Number },
    duration: { type: Number },
    exam_room: { type: Number },
    first_billed_date: { type: String },
    icd9_codes: { type: Array },
    icd10_codes: { type: Array },
    drChronoAppintmentID: { type: String },
    ins1_status: { type: String },
    ins2_status: { type: String },
    is_walk_in: { type: Boolean },
    is_virtual_base: { type: Boolean },
    last_billed_date: { type: String },
    notes: { type: String },
    office: { type: Number },
    patientID: { type: Number },
    primary_insurer_payer_id: { type: String },
    primary_insurer_name: { type: String },
    primary_insurance_id_number: { type: String },
    profile: { type: String },
    reason: { type: String },
    recurring_appointment: { type: Boolean },
    reminders: { type: Array },
    secondary_insurer_payer_id: { type: String },
    secondary_insurer_name: { type: String },
    secondary_insurance_id_number: { type: String },
    scheduled_time: { type: String },
    status: { type: String },
    status_transitions: { type: Array },
    supervising_provider: { type: Boolean },
    vitals: { type: Object },
    extended_updated_at: { type: String },
    zoomJoinURL: { type: String },
    zoomStartURL: { type: String },
    zoomMeetingID: { type: Number },
    zoomMeetingStatus: { type: String, default:"waiting" },
    zoomMeetingStartTime: { type: String },
    zoomMeetingEndTime: { type: String },
    zoomMeetingDuration: { type: Number },
    appointmentType: { type: String, default:"Appointment" },
    expiresAt: { type: Date, expires: 60*1*1*1}
},{
  timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Appointment', schema);
