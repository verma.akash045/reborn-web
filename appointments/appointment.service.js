const config = require('config.json');
﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const request = require('request');
var moment = require('moment');
const Doctor = db.Doctor;
const Drchrono = db.Drchrono;
const User = db.User;
const Appointment = db.Appointment;
const Zoom = db.Zoom;

module.exports = {
  createAppointment,
  cancelledAppointment,
  rescheduleAppointment,
  completeAppointment,
  getAppointments,
  setStartStatusToZoom,
  setEndStatusToZoom,
  confirmedAppointment
};

/**
 * API for get appointments with all the statusCodes
 */
async function getAppointments(userID) {
  const user = await User.findById(userID)
  let drChronoPatientID = user.patientID
  console.log(drChronoPatientID,"drChronoPatientID")
  let appointmentsArray = {}
  let currentDateTime = moment().format('YYYY-MM-DD HH:mm')

  let cancelledAppointment = await Appointment.find( {$and : [{ status:"Cancelled"},{ patientID:drChronoPatientID}] }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1, _id:1})
  let completeAppointment = await Appointment.find( {$and : [{ status:"Complete"},{ patientID:drChronoPatientID}] }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1, _id:1})
  let upcommingAppointment = await Appointment.find( {$and : [
                                                        { scheduled_time: { $gt: currentDateTime } },
                                                        { patientID:drChronoPatientID},
                                                        { status: { $nin : ["Cancelled", "Complete", "Unconfirmed"] } }
                                                      ]
                                                    }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1,zoomJoinURL:1, _id:1})

  appointmentsArray = await pushAppointmentIntoArray(cancelledAppointment, "cancelled", appointmentsArray)
  appointmentsArray = await pushAppointmentIntoArray(completeAppointment, "completed", appointmentsArray)
  appointmentsArray = await pushAppointmentIntoArray(upcommingAppointment, "upcomming", appointmentsArray)

  return appointmentsArray
}

async function pushAppointmentIntoArray(appointments, appointmentIndex, appointmentsArray){
  if(!appointmentsArray[appointmentIndex]){
    appointmentsArray[appointmentIndex] = []
  }
  await Promise.all(
    appointments.map(async appointment => {
      const appointmentObject = appointment.toObject();
      const doctor =  await Doctor.findOne({ doctorID: appointmentObject.doctorID }, {profile_picture: 1, first_name: 1, last_name: 1});
      const doctorObj = doctor;
      appointmentObject['doctor'] = {
          _id: doctorObj._id,
          name: `${doctorObj.first_name} ${doctorObj.last_name}`,
          profile_picture: doctorObj.profile_picture
        }
      appointmentsArray[appointmentIndex].push(appointmentObject)
    })
  );

  return appointmentsArray
}

/**
 * API for create appointments
 */

async function createAppointment(doctorID, userID, bodyParams) {

  if (!doctorID) {
    throw 'Doctor id is required'
  }

  if (!doctorID.match(/^[0-9a-fA-F]{24}$/)) {
    throw 'Doctor id not matched'
  }

  if (!bodyParams.duration) {
    throw 'Duration is required'
  }

  if (!bodyParams.examRoom) {
    throw 'Exam room is required'
  }

  if (!bodyParams.office) {
    throw 'office is required'
  }

  if (!bodyParams.appointmentDate) {
    throw 'Appointment date is required'
  }

  if (!bodyParams.appointmentTime) {
    throw 'Appointment time is required'
  }

  let date = moment(bodyParams.appointmentDate, "MM/DD/YYYY").format('YYYY-MM-DD');
  let currentDate = moment().format('YYYY-MM-DD')
  if(date == "Invalid date"){
    throw "Date should be in MM/DD/YYYY"
  }

  if(currentDate > date){
    throw "Date should be greater than current date"
  }

  if(bodyParams.appointmentTime.split(" ").length < 2){
    throw "Appointment time should be in format like 00:00 AM"
  }

  const doctor = await Doctor.findById(doctorID)
  if(!doctor || !doctor.doctorID){
    throw "Doctor not found"
  }

  let appointmentTemplate = await getDoctorAppointments(doctorID, date, date)
  let appointmentsAvailable = appointmentTemplate.appointmentsAvailable
  if(appointmentsAvailable.length < 1){
    throw "Appointment time is not available."
  }
  if(!appointmentsAvailable[0].timeSlots.includes(bodyParams.appointmentTime)){
    throw "Appointment time is not available."
  }

  const user = await User.findById(userID)
  if(!user || !user.patientID){
    throw "User not found on DRChrono."
  }

  let drChronoDoctorID = doctor.doctorID
  let drChronoPatientID = user.patientID
  let time = formatTime(bodyParams.appointmentTime)
  let scheduled_time = date+" "+time

  let currentDat = new Date()

  let appointmentParams = {
    duration: bodyParams.duration,
    exam_room: bodyParams.examRoom,
    office: bodyParams.office,
    scheduled_time: scheduled_time,
    status: "Unconfirmed",
    patientID: drChronoPatientID,
    doctorID: drChronoDoctorID,
    expiresAt: new Date(currentDat.getTime() + 4 * 60000)
  }

  const appointment = new Appointment(appointmentParams)
  await appointment.save()

  return await Appointment.findOne({_id:mongoose.Types.ObjectId(appointment.id) })

}

/**
 * API for confirmed appointments
 */

async function confirmedAppointment(appointmentID, userID, bodyParams) {

  if (!appointmentID) {
    throw 'Appointment id is required'
  }

  if (!appointmentID.match(/^[0-9a-fA-F]{24}$/)) {
    throw 'Appointment id not matched'
  }

  if (!bodyParams.status) {
    throw 'Status is required'
  }

  const appointmentData = await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) })

  if (!appointmentData || appointmentData.status != "Unconfirmed") {
    throw 'Appointment not found'
  }

  if (bodyParams.status != "success") {
    await Appointment.findByIdAndRemove(appointmentID)
    throw 'Appointment has been cancelled'
  }

  let date = moment(appointmentData.scheduled_time).format('YYYY-MM-DD');
  let time = moment(appointmentData.scheduled_time).format('HH:mm');

  const user = await User.findById(userID)
  const doctor = await Doctor.findOne({doctorID: appointmentData.doctorID})

  let drChronoDoctorID = appointmentData.doctorID
  let drChronoPatientID = user.patientID

  let scheduled_time = date+" "+time

  let scheduled_time_zoom = date+"T"+time
  let userName = await getUserName()
  let noteURL = "https://"+userName+".drchrono.com/patient/"+drChronoPatientID+"/integration/"+config.drchrono_integrationID+"/"

  // create meeting link for appointments.
  let zoomAppointment = await createOrUpdateZoomAppointmentLink(doctor, appointmentData.duration, scheduled_time_zoom)

  let requestBodyParams = {
    	"doctor":drChronoDoctorID,
    	"duration":appointmentData.duration,
    	"exam_room":appointmentData.exam_room,
    	"office":appointmentData.office,
    	"patient": drChronoPatientID,
    	"scheduled_time": scheduled_time,
      "notes": noteURL + " \r\n\r\n zoomURL: "+zoomAppointment.start_url
    }

    // Create appointments on DRChrono
    let createdAppointment =  await createOrUpdateAppointmentDRChrono(requestBodyParams, "", "")

    if(createdAppointment.id){

      createdAppointment.doctorID = createdAppointment.doctor
      createdAppointment.patientID = createdAppointment.patient
      createdAppointment.drChronoAppintmentID = createdAppointment.id
      createdAppointment.scheduled_time = moment(createdAppointment.scheduled_time).format("YYYY-MM-DD HH:mm:ss")
      createdAppointment.zoomJoinURL = zoomAppointment.join_url
      createdAppointment.zoomStartURL = zoomAppointment.start_url
      createdAppointment.zoomMeetingID = zoomAppointment.id
      createdAppointment.expiresAt = undefined
      delete createdAppointment.doctor
      delete createdAppointment.patient
      delete createdAppointment.id
      // save appointment in DB with all the data
      Object.assign(appointmentData, createdAppointment)
      await appointmentData.save()

      let saveAppointment = await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentData._id) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1,zoomJoinURL:1, _id:1})

      return saveAppointment
    }
}

/**
 * API for cancel appointments
 */
async function cancelledAppointment(appointmentID) {

  if (!appointmentID.match(/^[0-9a-fA-F]{24}$/)) {
    throw 'Appointment id not matched'
  }

  const appointmentData = await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1,zoomMeetingID:1, _id:1})
  if(!appointmentData || !appointmentData.doctorID){
    throw "Appointment not found"
  }
  // get doctors username from DRChrono
  let userName = await getUserName()

  let noteURL = "https://"+userName+".drchrono.com/patient/"+appointmentData.patientID+"/integration/"+config.drchrono_integrationID+"/"
  // DELETE meeting when we cancel the appointment
  let deleteMeeting = await deleteZoomAppointmentLink(appointmentData.zoomMeetingID)

  let requestBodyParams = {
    	"doctor":appointmentData.doctorID,
    	"duration":appointmentData.duration,
    	"exam_room":appointmentData.exam_room,
    	"office":appointmentData.office,
    	"patient": appointmentData.patientID,
    	"scheduled_time": appointmentData.scheduled_time,
    	"notes": noteURL,
      "status": "Cancelled",
      "reason": ""
    }

    let method = "PUT"
    // Cancel appointment on DRChrono
    let cancelAppointment = await createOrUpdateAppointmentDRChrono(requestBodyParams, appointmentData.drChronoAppintmentID, method)
    if(cancelAppointment == "success"){
      let appointmentParams = {
        "status": "Cancelled",
        "reason": "",
        "zoomJoinURL": "",
        "zoomStartURL": "",
        "notes": noteURL
      }
      // Save appointment in DB
      Object.assign(appointmentData, appointmentParams);
      await appointmentData.save();

      return await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1, _id:1})
    }
}

/**
 * API for reschedule appointments
 */
async function rescheduleAppointment(appointmentID, bodyParams) {

  if (!appointmentID.match(/^[0-9a-fA-F]{24}$/)) {
    throw 'Appointment id not matched'
  }

  if (!bodyParams.duration) {
    throw 'Duration is required'
  }

  if (!bodyParams.appointmentDate) {
    throw 'Appointment date is required'
  }

  if (!bodyParams.appointmentTime) {
    throw 'Appointment time is required'
  }

  let date = moment(bodyParams.appointmentDate, "MM/DD/YYYY").format('YYYY-MM-DD');
  let currentDate = moment().format('YYYY-MM-DD')
  if(date == "Invalid date"){
    throw "Date should be in MM/DD/YYYY"
  }

  if(currentDate > date){
    throw "Date should be greater than current date"
  }

  if(bodyParams.appointmentTime.split(" ").length < 2){
    throw "Appointment time should be in format like 00:00 AM"
  }

  const appointmentData = await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1,zoomJoinURL:1,zoomMeetingID:1, _id:1})
  if(!appointmentData || !appointmentData.doctorID){
    throw "Appointment not found"
  }
  let doctorID = appointmentData.doctorID
  // get doctor by doctorID
  const doctor = await Doctor.findOne({doctorID:doctorID})
  console.log(doctor._id,"doctor")
  let appointmentTemplate = await getDoctorAppointments(doctor._id, date, date)
  let appointmentsAvailable = appointmentTemplate.appointmentsAvailable
  if(appointmentsAvailable.length < 1){
    throw "Appointment time is not available."
  }
  if(!appointmentsAvailable[0].timeSlots.includes(bodyParams.appointmentTime)){
    throw "Appointment time is not available."
  }

  let time = formatTime(bodyParams.appointmentTime)
  let scheduled_time = date+" "+time
  let scheduled_time_zoom = date+"T"+time+"Z"
  let userName = await getUserName()


  let noteURL = "https://"+userName+".drchrono.com/patient/"+appointmentData.patientID+"/integration/"+config.drchrono_integrationID+"/"

  // update zoom appointment link
  await createOrUpdateZoomAppointmentLink(doctor, bodyParams.duration, scheduled_time_zoom, appointmentData.zoomMeetingID, "PATCH")
  // get zoom appointment details
  let zoomAppointment = await getZoomAppointmentLink(appointmentData.zoomMeetingID)

  let requestBodyParams = {
    	"doctor":appointmentData.doctorID,
    	"duration":bodyParams.duration,
    	"exam_room":appointmentData.exam_room,
    	"office":appointmentData.office,
    	"patient": appointmentData.patientID,
    	"scheduled_time": scheduled_time,
    	"notes": noteURL + " \r\n\r\n zoomURL: "+zoomAppointment.start_url,
      "status": "Rescheduled",
    }

    let method = "PUT"
    // Update DRChrono appointment
    let rescheduleAppointment = await createOrUpdateAppointmentDRChrono(requestBodyParams, appointmentData.drChronoAppintmentID, method)
    if(rescheduleAppointment == "success"){

      requestBodyParams.zoomJoinURL = zoomAppointment.join_url
      requestBodyParams.zoomStartURL = zoomAppointment.start_url
      requestBodyParams.zoomMeetingID = zoomAppointment.id
      // save appointment data in DB
      Object.assign(appointmentData, requestBodyParams);
      await appointmentData.save();

      return await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1,zoomJoinURL:1, _id:1})
    }
}

/**
 * API for complete appointments
 */
async function completeAppointment(appointmentID) {

  if (!appointmentID.match(/^[0-9a-fA-F]{24}$/)) {
    throw 'Appointment id not matched'
  }

  const appointmentData = await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1, _id:1})
  if(!appointmentData || !appointmentData.doctorID){
    throw "Appointment not found"
  }

  let requestBodyParams = {
    	"doctor":appointmentData.doctorID,
    	"duration":appointmentData.duration,
    	"exam_room":appointmentData.exam_room,
    	"office":appointmentData.office,
    	"patient": appointmentData.patientID,
    	"scheduled_time": appointmentData.scheduled_time,
    	"notes": appointmentData.notes,
      "status": "Complete",
    }

    let method = "PUT"
    // Complete appointment on DRChrono
    let completeAppointment = await createOrUpdateAppointmentDRChrono(requestBodyParams, appointmentData.drChronoAppintmentID, method)
    if(completeAppointment == "success"){

      Object.assign(appointmentData, requestBodyParams);
      await appointmentData.save();

      return await Appointment.findOne({_id:mongoose.Types.ObjectId(appointmentID) }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1, _id:1})
    }
}

/**
 * function for DELETE zoom meeting link
 */
async function deleteZoomAppointmentLink(zoomMeetingID) {
  let zoomURL = 'https://api.zoom.us/v2/meetings/'+zoomMeetingID
  let method = "DELETE"

  let zoomDetails = await Zoom.find();

  //Check if the DRChrono token exists
  if (zoomDetails && zoomDetails.length > 0) {

    let access_token = await getZoomAccessToken(zoomDetails)

    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': zoomURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token
      }
    };

    //Send hit for create appointment in DRChrono
  return await sendRequestZoomAsync(options).then(async jsonData => {
      if (jsonData) {
        return jsonData
      }else{
        return {}
      }
    })

  }

}

/**
 * function for GET zoom meeting link
 */
async function getZoomAppointmentLink(zoomMeetingID) {
  let zoomURL = 'https://api.zoom.us/v2/meetings/'+zoomMeetingID
  let method = "GET"

  let zoomDetails = await Zoom.find();

  //Check if the DRChrono token exists
  if (zoomDetails && zoomDetails.length > 0) {

    let access_token = await getZoomAccessToken(zoomDetails)

    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': zoomURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token
      }
    };

    //Send hit for create appointment in DRChrono
  return await sendRequestZoomAsync(options).then(async jsonData => {
      if (jsonData) {
        return jsonData
      }else{
        return {}
      }
    })

  }

}

/**
 * function for Update zoom meeting link
 */
async function createOrUpdateZoomAppointmentLink(doctor, duration, startTime, zoomMeetingID = "", method = "") {

  let zoomURL = 'https://api.zoom.us/v2/users/me/meetings'
  if(zoomMeetingID != ""){
    zoomURL = 'https://api.zoom.us/v2/meetings/'+zoomMeetingID
  }
  if(method == ""){
    method = "POST"
  }

  let zoomDetails = await Zoom.find();

  //Check if the DRChrono token exists
  if (zoomDetails && zoomDetails.length > 0) {

    let access_token = await getZoomAccessToken(zoomDetails)

    let topicAgenda = "Meeting with "+doctor.first_name+" "+doctor.last_name
    let doctorEmail = doctor.email
    if(config.zoom_email && config.zoom_email != ""){
      doctorEmail = config.zoom_email
    }
    bodyParams =
    {
      "topic": topicAgenda,
      "type": 2,
      "start_time": startTime,
      "duration": duration,
      "schedule_for": doctorEmail,
      "timezone": doctor.timezone,
      "password": "1234567890",
      "agenda": topicAgenda,
      "recurrence": {
        "type": "1",
        "repeat_interval": "",
        "weekly_days": "",
        "monthly_day": "",
        "monthly_week": "",
        "monthly_week_day": "",
        "end_times": "",
        "end_date_time": ""
      },
      "settings": {
        "host_video": 1,
        "participant_video": 1,
        "cn_meeting": false,
        "in_meeting": false,
        "join_before_host": true,
        "mute_upon_entry": false,
        "watermark": false,
        "use_pmi": false,
        "approval_type": 2,
        "registration_type": 1,
        "audio": "both",
        "auto_recording": "none",
        "enforce_login": 0,
        "enforce_login_domains": "",
        "alternative_hosts": "",
        "global_dial_in_countries": [ "" ],
        "registrants_email_notification": true
      }
    }
    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': zoomURL,
      'headers': {
        "Content-Type": ['application/json', 'application/json'],
        'Authorization': 'Bearer ' + access_token
      },
     body: JSON.stringify(bodyParams),
    };

    //Send hit for create appointment in DRChrono
  return await sendRequestZoomAsync(options).then(async jsonData => {
      if (jsonData) {
        return jsonData
      }else{
        return {}
      }
    })

  }

}


// Get access_token from zoom if expired and saved in DB OR Return access_token saved in DB
async function getZoomAccessToken(zoomDetails){

  let tokenExpireTimestamp = Date.parse(zoomDetails[0].expires_in)
  let currentDateTimestamp = Date.now()
  let access_token = "";
  //check if the token is expired
  if (tokenExpireTimestamp < currentDateTimestamp) {

    let bodyParams = {
      refresh_token: zoomDetails[0].refresh_token,
      grant_type: 'refresh_token',
    }
    // Here zoom_base64Token will be generated by format "Client ID:Client Secret"
    // Generate Base64 token using ​https://www.base64encode.net/
    var options = {
      'method': 'POST',
      'url': 'https://zoom.us/oauth/token',
      'headers': {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':'Basic '+config.zoom_base64Token
      },
      form: bodyParams,
    };
    //get new token and delete expired token
    await getZoomRefreshTokenAsync(options).then(async jsonData => {
      if (jsonData) {

        await Zoom.remove({}).then()

        var newDate = moment(moment().add(jsonData.expires_in, 'seconds')).format('YYYY-MM-DD HH:mm')
        access_token = jsonData.access_token
        let refresh_token = {
            	"access_token" : access_token,
            	"expires_in": newDate,
              "refresh_token":jsonData.refresh_token
            }
        console.log(refresh_token,"refresh_tokenrefresh_token")

        let zoom = new Zoom(refresh_token);
        // save zoom Token
        await zoom.save()
      }
    })
  } else {
    access_token = zoomDetails[0].access_token
  }

  return access_token
}


// send hit to get access_token from zoom if expired
const getZoomRefreshTokenAsync = (options) => {
  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error) {
        console.log(error)
        resolve()
      }
      jsonData = JSON.parse(body)
      if (jsonData.error) {
        console.log(jsonData.error)
        resolve()
      }

      resolve(jsonData);
    })
  })
}

/**
 * function for create or update appointment on drChrono
 */
async function createOrUpdateAppointmentDRChrono(bodyParams, appointmentID = "", method = "") {

  let appointmentURL = 'https://app.drchrono.com/api/appointments'
  if(appointmentID != ""){
    appointmentURL = 'https://app.drchrono.com/api/appointments/'+appointmentID
  }
  if(method == ""){
    method = "POST"
  }

  let drchronoDetails = await Drchrono.find();

  //Check if the DRChrono token exists
  if (drchronoDetails && drchronoDetails.length > 0) {

    let access_token = await getAccessToken(drchronoDetails)

    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': appointmentURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token
      },
      form: bodyParams,
    };

    //Send hit for create appointment in DRChrono
  return await sendRequestToDRChronoAsync(options).then(async jsonData => {
      if (jsonData) {
        return jsonData
      }else if(jsonData == "success"){
        return jsonData
      }else{
        return {}
      }
    })

  }

}

/**
 * function for GET username for noteURL
 */
async function getUserName() {
  let appointmentURL = 'https://app.drchrono.com/api/users/current'
  let method = "GET"
  let drchronoDetails = await Drchrono.find();

  //Check if the DRChrono token exists
  if (drchronoDetails && drchronoDetails.length > 0) {

    let access_token = await getAccessToken(drchronoDetails)

    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': appointmentURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token
      }
    };

    //Send hit for create appointment in DRChrono
  return await sendRequestToDRChronoAsync(options).then(async jsonData => {
      if (jsonData) {
        return jsonData.username
      }else{
        return []
      }
    })

  }
}

// send hit to create appointment from drChrono
const sendRequestToDRChronoAsync = (options) => {

  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if(response.statusCode == 204){
        resolve("success")
      }
      if (error || body == "" || body == " ") {
        resolve()
      } else {
        jsonData = JSON.parse(body)
        if (jsonData.error) resolve()

        resolve(jsonData);
      }
    })
  })
}

// send hit to create meeting from zoom
const sendRequestZoomAsync = (options) => {

  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error || body == "" || body == " ") {
        resolve()
      } else {
        jsonData = JSON.parse(body)
        if (jsonData.error) resolve()

        resolve(jsonData);
      }
    })
  })
}

// Get access_token from drChrono if expired and saved in DB OR Return access_token saved in DB
async function getAccessToken(drchronoDetails){

  let tokenExpireTimestamp = Date.parse(drchronoDetails[0].expires_in)
  let currentDateTimestamp = Date.now()
  let access_token = "";
  //check if the token is expired
  if (tokenExpireTimestamp < currentDateTimestamp) {

    let bodyParams = {
      refresh_token: drchronoDetails[0].refresh_token,
      grant_type: 'refresh_token',
      client_id: config.drchrono_client_id,
      client_secret: config.drchrono_client_secret,
    }
    var options = {
      'method': 'POST',
      'url': 'https://drchrono.com/o/token/',
      'headers': {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      form: bodyParams,
    };
    //get new token and delete expired token
    await getRefreshTokenAsync(options).then(async jsonData => {
      if (jsonData) {

        await Drchrono.remove({}).then()

        let date_ob = new Date();
        var newDate = new Date(date_ob.getTime() + (1000 * jsonData.expires_in))
        access_token = jsonData.access_token
        let refresh_token = {
            	"access_token" : access_token,
            	"expires_in":formatDate(newDate),
              "refresh_token":jsonData.refresh_token
            }
        console.log(refresh_token,"refresh_tokenrefresh_token")
        let drchrono = new Drchrono(refresh_token);
        // save DRchrono Token
        await drchrono.save()
      }
    })
  } else {
    access_token = drchronoDetails[0].access_token
  }

  return access_token
}


// send hit to get access_token from drChrono if expired
const getRefreshTokenAsync = (options) => {
  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error) {
        console.log(error)
        resolve()
      }
      jsonData = JSON.parse(body)
      if (jsonData.error) {
        console.log(jsonData.error)
        resolve()
      }

      resolve(jsonData);
    })
  })
}

function formatTime(time){

  let hours = Number(time.match(/^(\d+)/)[1])
  let minutes = Number(time.match(/:(\d+)/)[1])
  let AMPM = time.match(/\s(.*)$/)[1].toUpperCase()
  if(AMPM == "PM" && hours<12) hours = hours+12
  if(AMPM == "AM" && hours==12) hours = hours-12
  let sHours = hours.toString()
  var sMinutes = minutes.toString()
  if(hours<10) sHours = "0" + sHours
  if(minutes<10) sMinutes = "0" + sMinutes
  return sHours + ":" + sMinutes + ":00"

}

// function to set date in correct format for DRChrono
function formatDate(date) {
  return ('{0}-{1}-{3} {4}:{5}:{6}').replace('{0}', date.getFullYear()).replace('{1}', date.getMonth() + 1).replace('{3}', date.getDate()).replace('{4}', date.getHours()).replace('{5}', date.getMinutes()).replace('{6}', date.getSeconds())
}

async function getDoctorAppointments(doctorID, dateFrom, dateTo){

  const doctor = await Doctor.findById(doctorID)
  if(!doctor || !doctor.doctorID){
    throw "Doctor not found"
  }
  let dateRange = dateFrom+'/'+dateTo
  let dateRangeArray = dateRange.split('/')
  if(dateRangeArray && dateRangeArray.length != 2){
    throw "Date format is not correct, Please provide like: yyyy-mm-dd/yyyy-mm-dd"
  }

  let currentDate = moment().format('YYYY-MM-DD')

  if(dateRangeArray[0] < currentDate){
    throw "Start date should be greater or equals to current date"
  }
  if(dateRangeArray[1] <= currentDate){
    throw "End date should be greater current date"
  }

  let drID = doctor.doctorID

  let appointmentsArray = await getDoctorAppointmentsListDRChrono(drID, dateRange)
  let unConfirmedArray = await Appointment.find({$and : [{doctorID : drID}, {status: "Unconfirmed" }] });
  if(unConfirmedArray.length > 0){
    unConfirmedArray.forEach(function(unConfirmedAppointment){
      appointmentsArray.push(unConfirmedAppointment)
    })
  }

  let appointmentTemplate = await getDoctorAppointmentsTemplateDRChrono(drID)
  appointmentTemplate.week_days = await changeFormatWeekDays(appointmentTemplate.week_days)

  dateRangeArray = await getDates(dateRangeArray[0], dateRangeArray[1], appointmentTemplate.week_days)

  let startTime = appointmentTemplate.scheduled_time
  let endTime = getEndTime(appointmentTemplate.duration, appointmentTemplate.scheduled_time)
  let intervalMinutes = 30 // value should be in minutes
  let timeIntervals = await returnTimesInBetween(startTime, endTime, intervalMinutes)

  let updatedTimeInterval = []
  dateRangeArray.forEach(function(currentValue, index) {
    let timeSlots = timeIntervals
    if(currentValue == currentDate){
      let currentTime = moment().format('H:mm')
      let startH = parseInt(currentTime.split(":")[0]);
      let startM = parseInt(currentTime.split(":")[1]);
      timeSlots.forEach(function (timeSlot, timeSlotIndex){
        let timeSlotH = parseInt(timeSlot.split(":")[0]);
        let timeSlotM = parseInt(timeSlot.split(":")[1]);

        if( startH < timeSlotH ){
          updatedTimeInterval.push(timeSlot)
        }else if(startH == timeSlotH && startM < timeSlotM){
          updatedTimeInterval.push(timeSlot)
        }

      })
    }else{
      updatedTimeInterval = timeIntervals
    }
    dateRangeArray[index] = { date:currentValue, timeSlots:updatedTimeInterval}
  })

 // get appointments array with busy time slots
 let dateAppointmentTime = getFormatedAppointmensArray(appointmentsArray, intervalMinutes)

 dateRangeArray.forEach(function(dateRange, dateRangeIndex) {

    if(dateAppointmentTime[dateRange.date]){
      let appointmentsOnDate = dateAppointmentTime[dateRange.date]

       appointmentsOnDate.forEach(function (appointment, index) {
         dateRangeArray[dateRangeIndex].timeSlots = getFreeTimeSlots(appointment, dateRange.timeSlots)
       })
    }
    // Format date time in 12 hours
    dateRangeArray[dateRangeIndex].timeSlots = dateRangeArray[dateRangeIndex].timeSlots.map(getGenTime)
  })

  appointmentTemplate.appointmentsAvailable = dateRangeArray

  return appointmentTemplate
}

async function getDoctorAppointmentsListDRChrono(doctorID = '', dateRange = '', appointURL = null, appointmentArray = []) {

  let appointmentURL = ''
  if(appointURL != null){
    appointmentURL = appointURL;
  }else{
    appointmentURL = 'https://app.drchrono.com/api/appointments?page_size=1000&doctor='+doctorID+'&date_range='+dateRange
  }

  let method = "GET"
  let drchronoDetails = await Drchrono.find();

  //Check if the DRChrono token exists
  if (drchronoDetails && drchronoDetails.length > 0) {

    let access_token = await getAccessToken(drchronoDetails)

    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': appointmentURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token
      }
    };

    //Send hit for Get Doctors and update in DB
    await sendRequestForGetDoctorsAppointmentsAsync(options).then(async jsonData => {
      if (jsonData && jsonData.results) {
        let results = jsonData.results
        Object.keys(results).forEach(result => {
          appointmentArray.push(results[result])
        })
        console.log("I am Here")
        if(jsonData.next != null){
          await getDoctorAppointmentsListDRChrono('','',jsonData.next,appointmentArray)
        }
      }
    })
    return appointmentArray
  }
  return []
}


 // send hit to get Doctors appointment List from drChrono
 const sendRequestForGetDoctorsAppointmentsAsync = (options) => {

   return new Promise(resolve => {
     request(options, async function (error, response, body) {
       if (error || body == "" || body == " ") {
         resolve()
       } else {
         jsonData = JSON.parse(body)
         if (jsonData.error) resolve()

         resolve(jsonData);
       }
     })
   })
 }


  async function getDoctorAppointmentsTemplateDRChrono(doctorID) {

    let appointmentTemplateURL = 'https://app.drchrono.com/api/appointment_templates?doctor='+doctorID
    let method = "GET"
    let appointmentTemplateArray = {}
    let drchronoDetails = await Drchrono.find();

    //Check if the DRChrono token exists
    if (drchronoDetails && drchronoDetails.length > 0) {

      let access_token = await getAccessToken(drchronoDetails)

      // Params for get doctors list from DRChrono
      var options = {
        'method': method,
        'url': appointmentTemplateURL,
        'headers': {
          'Authorization': 'Bearer ' + access_token
        }
      };

      //Send hit for Get Doctors and update in DB
      await sendRequestForGetDoctorsAppointmentsTemplateAsync(options).then(async jsonData => {
        if (jsonData && jsonData.results) {
          appointmentTemplateArray = jsonData.results[0]
        }
      })
      return appointmentTemplateArray
    }
    return []
  }


   // send hit to get Doctors appointment template from drChrono
   const sendRequestForGetDoctorsAppointmentsTemplateAsync = (options) => {

     return new Promise(resolve => {
       request(options, async function (error, response, body) {
         if (error || body == "" || body == " ") {
           resolve()
         } else {
           jsonData = JSON.parse(body)
           if (jsonData.error) resolve()

           resolve(jsonData);
         }
       })
     })
   }



    async function changeFormatWeekDays(week_days) {
      let week_days_array = []
      for (i = 0; i < week_days.length; i++) {
           switch(week_days[i]) {
            case 0:
              week_days_array.push("Monday")
              break;
            case 1:
              week_days_array.push("Tuesday")
              break;
            case 2:
              week_days_array.push("Wednesday")
              break;
            case 3:
              week_days_array.push("Thursday")
              break;
            case 4:
              week_days_array.push("Friday")
              break;
            case 5:
              week_days_array.push("Saturday")
              break;
            default:
              week_days_array.push("Sunday")
          }

      }
      return week_days_array

    }



 async function getDates(startDate, stopDate, week_days) {
     var dateArray = [];
     var currentDate = moment(startDate);
     var stopDate = moment(stopDate);
     while (currentDate <= stopDate) {
         let dt = moment(currentDate, "YYYY-MM-DD")
         if(week_days.includes(dt.format('dddd'))){
           dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
         }
         currentDate = moment(currentDate).add(1, 'days')
     }
     return dateArray;
 }


 function getEndTime(duration, startTime, aMPM = ''){
   let date = new Date();
   var startH = parseInt(startTime.split(":")[0]);
   var startM = parseInt(startTime.split(":")[1]);

   date.setHours(startH, startM, 0);

   newDateObj = moment(date).add(duration, 'm').toDate();
   let minutes = (newDateObj.getMinutes() < 10) ? "0"+newDateObj.getMinutes() :  newDateObj.getMinutes()
   let endTime = newDateObj.getHours()+":"+minutes
   if(aMPM != ''){

   }else{
     return endTime
   }

 }



 function returnTimesInBetween(start, end, interval) {
    var timesInBetween = [];

    var startH = parseInt(start.split(":")[0]);
    var startM = parseInt(start.split(":")[1]);
    var endH = parseInt(end.split(":")[0]);
    var endM = parseInt(end.split(":")[1]);

    if(startM < 30 && endM < 30){
      for(let hour = startH; hour < endH; hour++) {
         timesInBetween.push(moment({ hour }).format('H:mm'));
         timesInBetween.push(
             moment({
                 hour,
                 minute: 30
             }).format('H:mm')
         );
     }
    }else{
      for (var i = startH; i <= endH; i++) {

        startBeforeH = parseInt(start.split(":")[0]);
        startBeforeM = start.split(":")[1];
        if(i == endH && startBeforeM >=  endM){

        }else{
          timesInBetween.push(i < 10 ? "0" + i + ":"+startBeforeM : start)
        }

        start = getEndTime(interval, start)
        startAfterH = parseInt(start.split(":")[0]);
        if(startBeforeH == startAfterH){
          i--
        }
      }

    }

    return timesInBetween;
  }


  function getFormatedAppointmensArray(appointmentsArray, intervalMinutes){

       let dateAppointmentTime = []
       appointmentsArray.forEach(function(appointment, index) {
         if(appointment.status != "Cancelled"){
              let date = moment(appointment.scheduled_time, "YYYY-MM-DD").format('YYYY-MM-DD')
              let startAppointmentTime = moment(appointment.scheduled_time).format('H:mm')
              let endAppointmentTime = moment(moment(appointment.scheduled_time).add(appointment.duration, 'minutes')).format('H:mm')
              let startH = parseInt(startAppointmentTime.split(":")[0]);
              let startM = parseInt(startAppointmentTime.split(":")[1]);
              let endH = parseInt(endAppointmentTime.split(":")[0]);
              let endM = parseInt(endAppointmentTime.split(":")[1]);
              let startTime = ''
              let endTime = ''
              if(startM >=0 && startM < 30 ){
                startTime = startH+":00"
              }else if (startM >= 30 && startM <= 59 ) {
                startTime = startH+":30"
              }else{
                startTime = startAppointmentTime
              }

              if(endM > 0 && endM < 30 ){
                endTime = endH+":30"
              }else if (endM >= 31 && endM <= 59 ) {
                endH = endH+1
                endTime = endH+":00"
              }else{
                endTime = endAppointmentTime
              }


              let timesInBetweenAppointments = returnTimesInBetween(startTime, endTime, intervalMinutes)

              let startEndArray = {startAppointmentTime, endAppointmentTime, InBetweenAppointment:JSON.stringify(timesInBetweenAppointments)}

              if(dateAppointmentTime[date]){
                dateAppointmentTime[date].push(startEndArray)
              }else{
                dateAppointmentTime[date] = [startEndArray]
              }
          }
        })

    return dateAppointmentTime
  }


  function getFreeTimeSlots(appointment, timeSlots){

      let timeSlotsArray = [...timeSlots]
      let removeValues = JSON.parse(appointment.InBetweenAppointment)

      removeValues.forEach(function(removeValue, Index){
        var startH = parseInt(removeValue.split(":")[0]);
        var startM = parseInt(removeValue.split(":")[1]);

        timeSlotsArray.forEach(function(timeSlot, index){
          var timeslotH = parseInt(timeSlot.split(":")[0]);
          var timeslotM = parseInt(timeSlot.split(":")[1]);
          // Remove start slots
          if(startH == timeslotH && startM == timeslotM){
            timeSlotsArray.splice(index,1)
          }else if(startH == timeslotH && startM > timeslotM ){
            if(timeSlotsArray[index+1]){
              var timeslotNextH = parseInt(timeSlotsArray[index+1].split(":")[0]);
              var timeslotNextM = parseInt(timeSlotsArray[index+1].split(":")[1]);
              if(startM < timeslotNextM){
                timeSlotsArray.splice(index,1);
              }else if(startM > timeslotNextM){
                timeSlotsArray.splice(index+1,1);
              }
            }
          }
        })
      })

      return timeSlotsArray
  }


   let getGenTime = (timeString) => {
     let timeH = parseInt(timeString.split(":")[0]);
     let timeM = timeString.split(":")[1];
     timeString = (timeH < 10) ? "0"+timeH+":"+timeM : timeString

     let H = +timeString.substr(0, 2);
     let h = (H % 12) || 12;
     let ampm = H < 12 ? " AM" : " PM";
     return timeString = h + timeString.substr(2, 3) + ampm;
   }

/**
* API for update start status of zoom meeting in appointment DB
*/
async function setStartStatusToZoom(bodyParams){

  let payloadObject = bodyParams.payload.object
  let appointmentData = await Appointment.findOne({zoomMeetingID: payloadObject.id})
  if(appointmentData && appointmentData.zoomMeetingID){
    let zoomParams = {
      zoomMeetingStatus: "started",
      zoomMeetingStartTime: moment().format("YYYY-MM-DD HH:mm:ss"),
      zoomMeetingDuration: payloadObject.duration,
    }
    // save appointment data in DB
    Object.assign(appointmentData, zoomParams);
    await appointmentData.save();
  }

}


/**
* API for update end status of zoom meeting in appointment DB
*/
async function setEndStatusToZoom(bodyParams){

  let payloadObject = bodyParams.payload.object
  let appointmentData = await Appointment.findOne({zoomMeetingID: payloadObject.id})
  if(appointmentData && appointmentData.zoomMeetingID){

    let startDate = moment(appointmentData.zoomMeetingStartTime, "YYYY-MM-DD HH:mm:ss")
    let endDate = moment().format("YYYY-MM-DD HH:mm:ss")
    let endDateFormat = moment(endDate, "YYYY-MM-DD HH:mm:ss")
    let duration = endDateFormat.diff(startDate, 'minutes')
    let zoomParams = {
      zoomMeetingStatus: "finished",
      zoomMeetingEndTime: endDate,
      zoomMeetingDuration: duration
    }

    // save appointment data in DB
    Object.assign(appointmentData, zoomParams);
    await appointmentData.save();
  }

}
