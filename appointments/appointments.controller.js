﻿const express = require('express');
const router = express.Router();
const appointmentService = require('./appointment.service');

// routes
// GET
router.get('/', getAppointments);
// POST
router.post('/create/:id', createAppointment);
router.post('/:id/confirmed', confirmedAppointment);
router.post('/:id/cancelled', cancelledAppointment);
router.post('/:id/reschedule', rescheduleAppointment);
router.post('/:id/complete', completeAppointment);
router.post('/zoomStatus', updateZoomStatus);

module.exports = router;
// Function is for create appointment
function createAppointment(req, res, next) {
  appointmentService.createAppointment(req.params.id, req.user.sub, req.body)
      .then(appointment => appointment ? res.status(200).json({ status: "success", appointment: appointment }) : res.status(400).json({ status: "error", message: "Error while booking appointment" }) )
      .catch(err => next(err));
}
// Function is for create appointment
function confirmedAppointment(req, res, next) {
  appointmentService.confirmedAppointment(req.params.id, req.user.sub, req.body)
      .then(appointment => appointment ? res.status(200).json({ status: "success", appointment: appointment }) : res.status(400).json({ status: "error", message: "Error while confirming appointment" }) )
      .catch(err => next(err));
}
// Function is for cancel appointment
function cancelledAppointment(req, res, next) {
  appointmentService.cancelledAppointment(req.params.id)
      .then(appointment => appointment ? res.status(200).json({ status: "success", appointment: appointment }) : res.status(400).json({ status: "error", message: "Error while cancelling appointment" }) )
      .catch(err => next(err));
}
// Function is for reschedule appointment
function rescheduleAppointment(req, res, next) {
  appointmentService.rescheduleAppointment(req.params.id, req.body)
      .then(appointment => appointment ? res.status(200).json({ status: "success", appointment: appointment }) : res.status(400).json({ status: "error", message: "Error while rescheduling appointment" }) )
      .catch(err => next(err));
}
// Function is for complete appointment
function completeAppointment(req, res, next) {
  appointmentService.completeAppointment(req.params.id, req.body)
      .then(appointment => appointment ? res.status(200).json({ status: "success", appointment: appointment }) : res.status(400).json({ status: "error", message: "Error while completing appointment" }) )
      .catch(err => next(err));
}
// Function is for get appointment's list
function getAppointments(req, res, next) {
  appointmentService.getAppointments(req.user.sub)
      .then(appointment => appointment ? res.json(appointment) : res.status(404).json({ status: "error", message: 'Appointments not found' }))
      .catch(err => next(err));
}

// Function is for update status links
function updateZoomStatus(req, res, next){

  if(req.body.event == "meeting.started"){

    appointmentService.setStartStatusToZoom(req.body).then().catch();
    res.status(200).json({ status: "success"})

  }else if (req.body.event == "meeting.ended") {

    appointmentService.setEndStatusToZoom(req.body).then().catch();
    res.status(200).json({ status: "success"})

  }else{
    res.status(200).json({ status: "success"})
  }

}
