const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const request = require('request');
var moment = require('moment');
var mongoose = require('mongoose');
const User = db.User;
const Drchrono = db.Drchrono;
const Appointment = db.Appointment;
const Doctor = db.Doctor;

module.exports = {
    getAll,
    deletePatientData,
    createPatient,
    updatePatient,
    getPatientsDetails
};

/**
 * API for get patients
 */
async function getAll() {

    let doctors =  await Doctor.find().sort({$natural: -1})
    let users = await User.find({ patientID: { $exists: true } }).sort({$natural: -1})
    let usersArray = []

    await Promise.all(
      users.map(async user => {
        user = user.toObject()
        if(user.doctorID && user.doctorID.match(/^[0-9a-fA-F]{24}$/)){
          let doctor = await Doctor.findById(user.doctorID)
          user.doctor = doctor
        }else{
          user.doctor = {}
        }

        usersArray.push(user)
      })
    );

    return {users: usersArray, doctors}
}

/**
 * API for get all data of patients
 */
async function getPatientsDetails(patientID) {
  let doctors =  await Doctor.find().sort({$natural: -1})
  const user = await User.findById(patientID)
  if(!user) throw "User not found"

  let appointments = ""
  let medications = ""
  if(user.patientID && user.patientID != ""){
    appointments = await getAppointments(user.patientID)
    medications = await getMedications(user.patientID)
  }

  return { user:user, appointments: appointments, medications:medications, doctors:doctors}
}

// function to get medications
async function getMedications(patientID, medicationURL = '', medications = []) {

  if(medicationURL == ""){
    medicationURL = 'https://app.drchrono.com/api/medications?&patient='+patientID
  }
  let method = "GET"

  let drchronoDetails = await Drchrono.find();

  //Check if the DRChrono token exists
  if (drchronoDetails && drchronoDetails.length > 0) {

    let tokenExpireTimestamp = Date.parse(drchronoDetails[0].expires_in)
    let currentDateTimestamp = Date.now()
    let access_token = "";
    //check if the token is expired
    if (tokenExpireTimestamp < currentDateTimestamp) {

      let bodyParams = {
        refresh_token: drchronoDetails[0].refresh_token,
        grant_type: 'refresh_token',
        client_id: config.drchrono_client_id,
        client_secret: config.drchrono_client_secret,
      }
      var options = {
        'method': 'POST',
        'url': 'https://drchrono.com/o/token/',
        'headers': {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        form: bodyParams,
      };
      //get new toket and delete expired token
      await getRefreshTokenAsync(options).then(async jsonData => {
        if (jsonData) {
          await Drchrono.remove({}).then()

          let date_ob = new Date();
          var newDate = new Date(date_ob.getTime() + (1000 * jsonData.expires_in))
          access_token = jsonData.access_token

          let refresh_token = {
              	"access_token" : access_token,
              	"expires_in":formatDate(newDate),
                "refresh_token":jsonData.refresh_token
              }

          let drchrono = new Drchrono(refresh_token);
          // save DRchrono Token
          await drchrono.save()
        }
      })
    } else {
      access_token = drchronoDetails[0].access_token
    }
    //Params for get Medications of patients
    var options = {
      'method': method,
      'url': medicationURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token,
        'Content-Type': 'application/json',
      }
    };

    //Send hit for get Medications of patients
    await sendRequestForPatientsAsync(options).then(async jsonData => {
      if(jsonData && jsonData.results){
        let results = jsonData.results

        Object.keys(results).forEach(result => {
          medications.push(results[result])
        })

        if(jsonData.next != null){
          await getMedications('',jsonData.next,medications)
        }

      }
    })
    return medications
  }
  return ""
}

// function to get appointments
async function getAppointments(drChronoPatientID) {

  let appointmentsArray = []
  let appointments = await Appointment.find({patientID: drChronoPatientID}).sort({scheduled_time: -1})
    await Promise.all(
      appointments.map(async appointment => {
        let appointmentObject = appointment.toObject();
        const doctor =  await Doctor.findOne({ doctorID: appointmentObject.doctorID }, {profile_picture: 1, first_name: 1, last_name: 1});
        const doctorObj = doctor;
        appointmentObject['doctor'] = {
            _id: doctorObj._id,
            name: `${doctorObj.first_name} ${doctorObj.last_name}`,
            profile_picture: doctorObj.profile_picture
          }

        if(appointmentsArray.length > 0){
          appointmentsArray.push(appointmentObject)
        }else{
          appointmentsArray = [appointmentObject]
        }
      })
    );

    return appointmentsArray;
}

/**
 * API for delete data of patients
 */
async function deletePatientData(patientID) {

  const user = await User.findById(patientID)
  if(!user) throw "User not found"
  let appointments = Appointment.find({patientID:user.patientID})
  if(appointments.length > 0){
    await Appointment.remove({patientID:user.patientID}).then()
  }
  await User.findByIdAndRemove(patientID);

}

/**
 * API for create data of patients
 */
async function createPatient(userParam) {

  if (!userParam.email) {
    throw 'Email is required'
  }
  userParam.email = userParam.email.toLowerCase()

  let userCheck = await User.findOne({ email: userParam.email }).select('-hash');

  if (userCheck && userCheck.email == userParam.email) {
    throw 'Email "' + userParam.email + '" is already taken';
  }

  if (!userParam.password) {
    throw 'Password is required'
  }
  var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
  if (!passwordRegex.test(userParam.password)) {
    throw 'Password must be greater than or equals to 8 characters, Contains 1 uppercase, 1 lowercase and 1 number'
  }

  userParam.hash = bcrypt.hashSync(userParam.password, 10);

  let firstName = ''
  let lastName = ''

  if (userParam.firstName) {
      firstName = userParam.firstName
  }

  if (userParam.lastName) {
      lastName = " "+userParam.lastName
  }
  // Handeled name on our side as we are using name in various places
  userParam.name = firstName+""+lastName

  console.log(userParam, "userParam")
  const user = new User(userParam)
  await user.save()

  return {
    status: "success",
    message: "User created successfully"
  };
}

/**
 * API for update data of patients
 */
async function updatePatient(userParam, patientID) {

  const user = await User.findById(patientID)
  userParam = await clearUserPassword(userParam)

  // validate
  if (!user) throw 'User not found';

  if (!userParam.email) {
    throw 'Email is required'
  }
  userParam.email = userParam.email.toLowerCase()

  let userCheck = await User.findOne({$and : [
                                                  { email:userParam.email},
                                                  { _id: { $nin :mongoose.Types.ObjectId(patientID)  } }
                                                ]}
                                              );

  if (userCheck && userCheck.email == userParam.email) {
    throw 'Email "' + userParam.email + '" is already taken';
  }

  if (userParam.password && userParam.password != "") {

    var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
    if (!passwordRegex.test(userParam.password)) {
      throw 'Password must be greater than or equals to 8 characters, Contains 1 uppercase, 1 lowercase and 1 number'
    }

    userParam.hash = bcrypt.hashSync(userParam.password, 10);

  }


  let firstName = ''
  let lastName = ''

  if (userParam.firstName) {
      firstName = userParam.firstName
  }

  if (userParam.lastName) {
      lastName = " "+userParam.lastName
  }
  // Handeled name on our side as we are using name in various places
  userParam.name = firstName+""+lastName
  userParam.dob = moment(userParam.dob).format('YYYY-MM-DD');

  //copy userParam properties to user
  Object.assign(user, userParam);
  await user.save()

  return {
    status: "success",
    message: "User updated successfully"
  };
}

// Function to remove null and blank values from user params
async function clearUserPassword(userParams){
  for (var propName in userParams) {
    console.log(propName)
    if (propName == "password" && userParams[propName] == "") {
      delete userParams[propName];
    }
  }

  return userParams;
}

// Function to remove null and blank values from user params
async function clearUserObject(userParams){
  for (var propName in userParams) {
    if (userParams[propName] === null || userParams[propName] === undefined || userParams[propName] == "") {
      delete userParams[propName];
    }
  }

  return userParams;
}

// promise for send hit to DRChrono to create or update patient
const sendRequestForPatientsAsync = (options) => {

  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error || body == "" || body == " ") {
        resolve()
      } else {
        jsonData = JSON.parse(body)
        if (jsonData.error) resolve()

        resolve(jsonData);
      }
    })
  })
}

// Send hit to DRChrono for get auth_code by referes token
const getRefreshTokenAsync = (options) => {
  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error) {
        resolve()
      }
      jsonData = JSON.parse(body)
      if (jsonData.error) resolve()

      resolve(jsonData);
    })
  })
}

// format date after adding expiry date
function formatDate(date) {
  return ('{0}-{1}-{3} {4}:{5}:{6}').replace('{0}', date.getFullYear()).replace('{1}', date.getMonth() + 1).replace('{3}', date.getDate()).replace('{4}', date.getHours()).replace('{5}', date.getMinutes()).replace('{6}', date.getSeconds())
}
