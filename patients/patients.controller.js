﻿const express = require('express');
const router = express.Router();
const session = require('express-session')
var moment = require('moment');
const patientService = require('./patient.service');

// routes
router.get('/', getAll);
router.get('/details/:patientID', getPatientsDetails);

// POST
router.post('/create', createPatient);
router.post('/update', updatePatient);

// DELETE
router.delete('/delete', deletePatientData);

module.exports = router;

// Function for get all goals
function getAll(req, res, next) {
  if(req.session.token)
{
  patientService.getAll()
  .then(patients => patients ? res.render('patients/patients', {page:'Patients', patients:patients.users, doctors:patients.doctors, moment:moment, error:""}) : res.render('patients', {page:'Patients', patients:"Patients not found", moment:moment, doctors:[], error:""} ) )
  .catch(err => res.render('patients/patients', {page:'Patients', moment:moment, patients:[], doctors: [], error:err}));
}else{
  res.redirect('/adminLogin')
}   
}

// Function for get delete patient
function deletePatientData(req, res, next) {
  patientService.deletePatientData(req.body.patientID)
    .then(user => user ? res.json(user) : res.json([]) )
    .catch(err => next(err) );
}

// Function for create patients
function createPatient(req, res, next) {
    patientService.createPatient(req.body)
      .then(patients => patients ? res.json(patients) : res.json([]) )
      .catch(err => next(err) );
}

// Function for create patients
function updatePatient(req, res, next) {
    patientService.updatePatient(req.body, req.query.patientID)
      .then(patients => patients ? res.json(patients) : res.json([]) )
      .catch(err => next(err) );
}

// Function for create patients
function getPatientsDetails(req, res, next) {
  if(req.session.token){
    patientService.getPatientsDetails(req.params.patientID)
    .then(patient => patient ? res.render('patients/patientDetails', {page:'Patient Details', patient:patient.user, medications:patient.medications, appointments:patient.appointments, doctors:patient.doctors, moment:moment, error:""}) : res.render('patientDetails', {page:'Patient Details', patient:"Patient not found", appointments:"", doctors:[], medications:"", moment:moment, error:""} ) )
    .catch(err => res.render('patients/patientDetails', {page:'Patient Details', moment:moment, medications:"", doctors:[], patient:{}, appointments:"", error:err}));
  }else{
    res.redirect('/adminLogin')
  }
    
}
