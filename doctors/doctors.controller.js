﻿const express = require('express');
const router = express.Router();
const session = require('express-session')
const doctorService = require('./doctor.service');
var moment = require('moment');
var multer  =   require('multer');

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './public/uploads/doctors');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
  }
});

var upload = multer({ storage : storage}).single('profile_picture');
// routes

//GET
router.get('/saveList', saveDoctorList);
router.get('/', getDoctorList);
router.get('/getDoctors', getDoctorsData);
router.get('/details/:docID', getDoctorDataByID);
router.get('/detail/:docID', getDoctorDetailsByID);
router.get('/all', getAllDoctorList);
router.get('/appointments/:id', getDoctorAppointments);
// POST
router.post('/assignGoals/:id', assignGoals);
router.post('/addDoctors', addDoctorsData);
router.post('/updateDoctors', updateDoctorForm);
router.post('/updateSessions', updateSessions);
router.post('/addSessions', addSessions);
router.post('/updateTemplate', updateTemplate);

// PUT
router.patch('/:id', updateDoctor);

// DELETE
router.delete('/deleteDoctors', deleteDoctorsData);


module.exports = router;

// Function is for save doctor list from DRChrono to our DB
function saveDoctorList(req, res, next) {
    doctorService.saveDoctorList()
        .then(doctors => doctors ? res.status(200).json({ status: "success", doctors: doctors }) : res.status(400).json({ status: "error", doctors: [] }) )
        .catch(err => next(err));
}

// Function is for assign goals to Doctors
function assignGoals(req, res, next) {
    doctorService.assignGoalsToDoctor(req.body, req.params.id)
        .then(doctors => doctors ? res.status(200).json({ status: "success", doctor: doctors }) : res.status(400).json({ status: "error", message: "Error while assigning goals" }) )
        .catch(err => next(err));
}

// Function is for update doctors details in our DB
function updateDoctor(req, res, next) {
    doctorService.updateDoctor(req.body, req.params.id)
        .then(doctors => doctors ? res.status(200).json({ status: "success", doctor: doctors }) : res.status(400).json({ status: "error", message: "Error while updating doctor" }) )
        .catch(err => next(err));
}

// Function is for get doctors list according to the assign goals
function getDoctorList(req, res, next) {
    doctorService.getDoctorList(req.user.sub)
        .then(doctors => doctors ? res.json(doctors) : res.json([]) )
        .catch(err => next(err));
}

// Function is for get all doctors list
function getAllDoctorList(req, res, next) {
    doctorService.getAllDoctorList()
        .then(doctors => doctors ? res.json(doctors) : res.json([]) )
        .catch(err => next(err));
}

// Function is for get available slots and working week days according to appointments
function getDoctorAppointments(req, res, next) {
    doctorService.getDoctorAppointments(req.params.id, req.query.dateFrom, req.query.dateTo)
        .then(doctorAppointments => doctorAppointments ? res.json(doctorAppointments) : res.json([]) )
        .catch(err => next(err));
}

// Function for get addDoctors and list doctors
function getDoctorsData(req, res, next) {
  if(req.session.token){
    doctorService.getAllDoctorListWeb()
    .then(user => user ? res.render('doctors/addDoctors', {page:'Add Doctors', doctors:user.doctorArray, clinics:user.clinics, error:""}) : res.render('addDoctors', {page:'Add Doctors', doctors:"Doctors not found", clinics:[], error:""} ) )
    .catch(err => res.render('doctors/addDoctors', {page:'Add Doctors', doctors:[], clinics:[], error:err}));
  }else{
    res.redirect('/adminLogin')
  }
}

// Function for get addDoctors and list doctors
function getDoctorDataByID(req, res, next) {
  if(req.session.token){
    doctorService.getAllDoctoByIDWeb(req.params.docID)
    .then(user => user ? res.render('doctors/doctorDetails', {page:'Doctor Details', tab:req.query.tab, doctor:user.doctor, clinics:user.clinics, patients:user.patientsArray, appointmentTemplate:user.appointmentTemplate, moment:moment, error:""}) : res.render('doctorDetails', {page:'Doctor Details', tab:"", doctor:"Doctor not found", clinics:[], patients:[], appointmentTemplate:{}, moment:moment, error:""} ) )
    .catch(err => res.render('doctors/doctorDetails', {page:'Doctor Details', tab:"", doctor:{}, clinics: [], patients:[], appointmentTemplate:{}, moment:moment, error:err}));
}
  else{
    res.redirect('/adminLogin')
  }
}

// Function for get addDoctors and list doctors
function getDoctorDetailsByID(req, res, next) {
  doctorService.getDoctorDetailsByID(req.params.docID)
    .then(user => user ? res.json(user) : res.json([]) )
    .catch(err => next(err) );
}

// Function for get addDoctors and list doctors
function addDoctorsData(req, res, next) {
  upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        proPic = ''
        if(req.file && req.file.path){
          proPic = req.file.path
        }
        doctorService.addDoctorsData(req.body, proPic)
          .then(user => user ? res.json(user) : res.json([]) )
          .catch(err => next(err) );
    });
}

// Function for get addDoctors and list doctors
function updateDoctorForm(req, res, next) {

  upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        proPic = ''
        if(req.file && req.file.path){
          proPic = req.file.path
        }
        doctorService.updateDoctorsDataForm(req.body, proPic)
          .then(user => user ? res.json(user) : res.json([]) )
          .catch(err => next(err) );
    });
}

// Function for get addDoctors and list doctors
function updateTemplate(req, res, next) {
    doctorService.updateTemplate(req.body, req.query.docID)
      .then(user => user ? res.json(user) : res.json([]) )
      .catch(err => next(err) );
}

// Function for get addDoctors and list doctors
function updateSessions(req, res, next) {
    doctorService.updateSessions(req.body, req.query.docID)
      .then(user => user ? res.json(user) : res.json([]) )
      .catch(err => next(err) );
}

// Function for get addDoctors and list doctors
function addSessions(req, res, next) {
    doctorService.addSessions(req.body, req.query.docID)
      .then(user => user ? res.json(user) : res.json([]) )
      .catch(err => next(err) );
}

// Function for get deleteDoctor
function deleteDoctorsData(req, res, next) {
  doctorService.deleteDoctorsData(req.body.docID)
    .then(user => user ? res.json(user) : res.json([]) )
    .catch(err => next(err) );
}
