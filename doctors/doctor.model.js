const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  doctorID: { type: String },
  first_name: { type: String  },
  last_name: { type: String },
  email: { type: String },
  specialty: { type: String },
  job_title: { type: String },
  suffix: { type: String },
  website: { type: String },
  home_phone: { type: String },
  office_phone: { type: String },
  cell_phone: { type: String },
  country: { type: String },
  timezone: { type: String },
  npi_number: {type: String},
  group_npi_number: {type: String},
  practice_group: {type: String},
  practice_group_name: {type: String},
  profile_picture: {type: String, default:''},
  is_account_suspended: {type: Boolean, default: false},
  goals: [{ type: String, ref: 'Goal' }],
  education: {type: String},
  ratings: {type: Number},
  clinicID: {type: String, ref: 'Clinic'},
  clinicName: {type: String},
  sessions: {type: Array, default:[]},
}, {
    timestamps: true
  });

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Doctor', schema);
