﻿const config = require('config.json');
const db = require('_helpers/db');
const request = require('request');
const https = require('https');
const fs = require('fs');
var mongoose = require('mongoose');
var moment = require('moment');
const Doctor = db.Doctor;
const Drchrono = db.Drchrono;
const User = db.User;
const Clinic = db.Clinic;
const Appointment = db.Appointment;

module.exports = {
    saveDoctorList,
    assignGoalsToDoctor,
    getDoctorList,
    getDoctorAppointments,
    updateDoctor,
    getAllDoctorList,
    addDoctorsData,
    deleteDoctorsData,
    updateDoctorsDataForm,
    getAllDoctorListWeb,
    getAllDoctoByIDWeb,
    updateSessions,
    addSessions,
    updateTemplate,
    getDoctorDetailsByID
};


/**
 * API for get doctors from drchrono and save into our DB
 */
async function saveDoctorList() {
  await getDoctorListDRChronoAndSave()
  let doctors = await Doctor.find().populate( {path:'goals', select:["title", "image"]} )

  return doctors
}

// Get the DR List and saved in DB
async function getDoctorListDRChronoAndSave() {

  let doctorURL = 'https://app.drchrono.com/api/doctors?page_size=1000'
  let method = "GET"
  let drchronoDetails = await Drchrono.find();

  //Check if the DRChrono token exists
  if (drchronoDetails && drchronoDetails.length > 0) {

    let access_token = await getAccessToken(drchronoDetails)

    // Params for get doctors list from DRChrono
    var options = {
      'method': method,
      'url': doctorURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token
      }
    };

    //Send hit for Get Doctors and update in DB
    await sendRequestForGetDoctorsAsync(options).then(async jsonData => {
      if (jsonData && jsonData.results) {
        let doctorResults = await getDoctorsResults(jsonData.results)
        console.log(doctorResults)
        if(doctorResults.length > 0){
          await Doctor.insertMany(doctorResults, function(err, res) { });
        }
      }
    })

  }

}
/**
 * function for GET doctors results
 */
async function getDoctorsResults(doctorResults){
  let doctorArray = []
  await Promise.all(
    doctorResults.map(async (doctor, doctorIndex) => {

      doctorResults[doctorIndex].doctorID = doctor.id
      delete doctorResults[doctorIndex].id

      let doctors = await Doctor.findOne({doctorID:doctorResults[doctorIndex].doctorID})
      // console.log(doctors)
      if(doctors && doctors.email){
        doctorResult = doctors.toObject()
        doctorResults[doctorIndex].goals = doctorResult.goals
        if(doctors.profile_picture == '' && doctorResults[doctorIndex].profile_picture != ''){
          let imageName = doctorResults[doctorIndex].doctorID+'_'+Date.now()+'.png';
          await saveImageToFolder(doctorResults[doctorIndex].profile_picture,imageName);
          // doctorResults[doctorIndex].profile_picture = profile_picture.replace('./public/', '')
          doctorResults[doctorIndex].profile_picture = 'uploads/doctors/'+imageName
        }else{
          doctorResults[doctorIndex].profile_picture = doctorResult.profile_picture
        }
        Object.assign(doctors, doctorResults[doctorIndex]);
        await doctors.save()
      }else{
        doctorArray.push( doctorResults[doctorIndex] )
      }
    })
  );
  return doctorArray
}
/**
 * function for save doctor image to our server
 */
async function saveImageToFolder(url, imageName){

  let dirpath = './public/uploads/doctors/'
  if(!fs.existsSync(dirpath)){
    fs.mkdirSync(dirpath, { recursive: true })
  }
  let localPath = dirpath+''+imageName
  var file = fs.createWriteStream(localPath);

  // return new Promise((resolve, reject) => {
    https.get(url, function(response) {
      response.pipe(file)
      // .on('close', () => resolve(localPath))
    })
  // })
}

// Get access_token from drChrono if expired and saved in DB OR Return access_token saved in DB
async function getAccessToken(drchronoDetails){

  let tokenExpireTimestamp = Date.parse(drchronoDetails[0].expires_in)
  let currentDateTimestamp = Date.now()
  let access_token = "";
  //check if the token is expired
  if (tokenExpireTimestamp < currentDateTimestamp) {

    let bodyParams = {
      refresh_token: drchronoDetails[0].refresh_token,
      grant_type: 'refresh_token',
      client_id: config.drchrono_client_id,
      client_secret: config.drchrono_client_secret,
    }
    var options = {
      'method': 'POST',
      'url': 'https://drchrono.com/o/token/',
      'headers': {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      form: bodyParams,
    };
    //get new token and delete expired token
    await getRefreshTokenAsync(options).then(async jsonData => {
      if (jsonData) {

        await Drchrono.remove({}).then()

        let date_ob = new Date();
        var newDate = new Date(date_ob.getTime() + (1000 * jsonData.expires_in))
        access_token = jsonData.access_token
        let refresh_token = {
            	"access_token" : access_token,
            	"expires_in":formatDate(newDate),
              "refresh_token":jsonData.refresh_token
            }
        console.log(refresh_token,"refresh_tokenrefresh_token")
        let drchrono = new Drchrono(refresh_token);
        // save DRchrono Token
        await drchrono.save()
      }
    })
  } else {
    access_token = drchronoDetails[0].access_token
  }

  return access_token
}

// send hit to get Doctors List from drChrono
const sendRequestForGetDoctorsAsync = (options) => {

  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error || body == "" || body == " ") {
        resolve()
      } else {
        jsonData = JSON.parse(body)
        if (jsonData.error) resolve()

        resolve(jsonData);
      }
    })
  })
}

// send hit to get access_token from drChrono if expired
const getRefreshTokenAsync = (options) => {
  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error) {
        resolve()
      }
      jsonData = JSON.parse(body)
      if (jsonData.error) {
        resolve()
      }

      resolve(jsonData);
    })
  })
}

// function to set date in correct format for DRChrono
function formatDate(date) {
  return ('{0}-{1}-{3} {4}:{5}:{6}').replace('{0}', date.getFullYear()).replace('{1}', date.getMonth() + 1).replace('{3}', date.getDate()).replace('{4}', date.getHours()).replace('{5}', date.getMinutes()).replace('{6}', date.getSeconds())
}

/**
 * API to assign goals to doctors
 */
 async function assignGoalsToDoctor(bodyParams, doctorID){

   if (!doctorID) {
     throw 'Doctor id is required'
   }

   if (!bodyParams.goals || bodyParams.goals.length < 1) {
     throw 'Goals are required'
   }
   if (!doctorID.match(/^[0-9a-fA-F]{24}$/)) {
     throw 'Doctor id not matched'
   }

   const doctor = await Doctor.findById(doctorID)
   if(!doctor || !doctor.doctorID){
     throw "Doctor not found"
   }

   Object.assign(doctor, bodyParams);
   if(await doctor.save()){
     return await Doctor.findById(doctorID).populate( {path:'goals', select:["_id","title", "image"]} );
   }
 }

/**
 * API to update doctors
 */
 async function updateDoctor(bodyParams, doctorID){

   if (!doctorID) {
     throw 'Doctor id is required'
   }

   if (!doctorID.match(/^[0-9a-fA-F]{24}$/)) {
     throw 'Doctor id not matched'
   }

   const doctor = await Doctor.findById(doctorID)
   if(!doctor || !doctor.doctorID){
     throw "Doctor not found"
   }

   Object.assign(doctor, bodyParams);
   if(await doctor.save()){
     return await Doctor.findById(doctorID).populate( {path:'goals', select:["_id","title", "image"]} );
   }
 }

 /**
  * API to get doctor list from DB according to curret goals in user
  */
 async function getDoctorList(userID){

   let user = await User.findById(userID).select('-hash')
   if (!user.goals || user.goals.length < 1) {
     throw "Users has no goals yet"
   }

   let goalsArray =  await getGoalIDs(user)

   let doctors = await Doctor.find({$and: [{ doctorID: { $exists: true } }, {doctorID:{$ne:""}}, { $or: goalsArray } ] }).populate( {path:'goals', select:["title", "image"]} )

   return doctors
 }

 /**
  * API to get doctor list from DB
  */
 async function getAllDoctorList(){

   let doctors = await Doctor.find({$and: [{ doctorID: { $exists: true } }, {doctorID:{$ne:""}} ] }).sort({$natural: -1}).populate( {path:'goals', select:["title", "image"]} )

   return doctors
 }

 /**
  * API to get doctor list from DB
  */
 async function getAllDoctorListWeb(){

   let doctors = await Doctor.find().sort({$natural: -1}).populate( [{path:'goals', select:["title", "image"]},{path:'clinicID', select:["title"]}]  )
   let clinics = await Clinic.find().sort({$natural: -1})

   let doctorArray = []

   await Promise.all(
     doctors.map(async doctor => {
       doctor = doctor.toObject()
       let doctorPtientsCount = await User.find({doctorID:doctor._id}).count()
       doctor.patientsCount = doctorPtientsCount
       doctorArray.push(doctor)
     })
   );

   return {doctorArray, clinics}
 }

 /**
  * API to get doctor by ID from DB
  */
 async function getAllDoctoByIDWeb(docID){

   let doctor = await Doctor.findById( docID ).populate( [{path:'clinicID', select:["title"]}]  )
   let clinics = await Clinic.find().sort({$natural: -1})
   let doctorPatients = await User.find({doctorID:doctor._id})
   let appointmentTemplate = await getDoctorAppointmentsTemplateDRChrono(doctor.doctorID)

   let patientsArray = []

   await Promise.all(
     doctorPatients.map(async doctorPatient => {
       doctorPatient = doctorPatient.toObject()
       let patientsSessionsCount = await Appointment.find({patientID:doctorPatient.patientID}).count()
       doctorPatient.sessionsCount = patientsSessionsCount
       patientsArray.push(doctorPatient)
     })
   );

   return {doctor, patientsArray, clinics, appointmentTemplate}
 }

 /**
  * API to get doctor by ID from DB
  */
 async function getDoctorDetailsByID(docID){

    if(!docID){
      throw "Doctor ID is required"
    }

    if (!docID.match(/^[0-9a-fA-F]{24}$/)) {
      throw 'Please enter a valid id'
    }

   let doctor = await Doctor.findById( docID ).populate( [{path:'clinicID', select:["title"]}]  )
   if(!doctor || !doctor._id){
     throw "Doctor not found"
   }
   
   return doctor
 }

 /**
  * API to remove doctor
  */
 async function deleteDoctorsData(doctorId){
     await Doctor.findByIdAndRemove(doctorId);

     return {status:"success", message:"doctor removed"}
 }

// Get Goals ID array
 async function getGoalIDs(user) {
      let goalArray = []
     await Promise.all(
       user.goals.map(async item => {
          let goalObj = {goals: item.id};
          goalArray.push(goalObj)
       })
     );
   return goalArray
 }


 /**
  * API to get doctor appointments list from DRChrono according to DR ID
  */
 async function getDoctorAppointments(doctorID, dateFrom, dateTo){

   if (!doctorID.match(/^[0-9a-fA-F]{24}$/)) {
     throw 'Please enter a valid id'
   }

   const doctor = await Doctor.findById(doctorID)
   if(!doctor || !doctor.doctorID){
     throw "Doctor not found"
   }
   let dateRange = dateFrom+'/'+dateTo
   let dateRangeArray = dateRange.split('/')
   if(dateRangeArray && dateRangeArray.length != 2){
     throw "Date format is not correct, Please provide like: yyyy-mm-dd/yyyy-mm-dd"
   }

   let currentDate = moment().format('YYYY-MM-DD')

   if(dateRangeArray[0] < currentDate){
     throw "Start date should be greater or equals to current date"
   }
   if(dateRangeArray[1] <= currentDate){
     throw "End date should be greater current date"
   }

   let drID = doctor.doctorID

   let appointmentsArray = await getDoctorAppointmentsListDRChrono(drID, dateRange)
   let unConfirmedArray = await Appointment.find({$and : [{doctorID : drID}, {status: "Unconfirmed" }] });
   if(unConfirmedArray.length > 0){
     unConfirmedArray.forEach(function(unConfirmedAppointment){
       appointmentsArray.push(unConfirmedAppointment)
     })
   }

   let appointmentTemplate = await getDoctorAppointmentsTemplateDRChrono(drID)
   appointmentTemplate.week_days = await changeFormatWeekDays(appointmentTemplate.week_days)

   dateRangeArray = await getDates(dateRangeArray[0], dateRangeArray[1], appointmentTemplate.week_days)

   let startTime = appointmentTemplate.scheduled_time
   let endTime = getEndTime(appointmentTemplate.duration, appointmentTemplate.scheduled_time)
   let intervalMinutes = 30 // value should be in minutes
   let timeIntervals = await returnTimesInBetween(startTime, endTime, intervalMinutes)

   let updatedTimeInterval = []
   dateRangeArray.forEach(function(currentValue, index) {
     let timeSlots = timeIntervals
     if(currentValue == currentDate){
       let currentTime = moment().format('H:mm')
       let startH = parseInt(currentTime.split(":")[0]);
       let startM = parseInt(currentTime.split(":")[1]);
       timeSlots.forEach(function (timeSlot, timeSlotIndex){
         let timeSlotH = parseInt(timeSlot.split(":")[0]);
         let timeSlotM = parseInt(timeSlot.split(":")[1]);

         if( startH < timeSlotH ){
           updatedTimeInterval.push(timeSlot)
         }else if(startH == timeSlotH && startM < timeSlotM){
           updatedTimeInterval.push(timeSlot)
         }

       })
     }else{
       updatedTimeInterval = timeIntervals
     }
     dateRangeArray[index] = { date:currentValue, timeSlots:updatedTimeInterval}
   })

  // get appointments array with busy time slots
  let dateAppointmentTime = getFormatedAppointmensArray(appointmentsArray, intervalMinutes)

  dateRangeArray.forEach(function(dateRange, dateRangeIndex) {

     if(dateAppointmentTime[dateRange.date]){
       let appointmentsOnDate = dateAppointmentTime[dateRange.date]

        appointmentsOnDate.forEach(function (appointment, index) {
          dateRangeArray[dateRangeIndex].timeSlots = getFreeTimeSlots(appointment, dateRange.timeSlots)
        })
     }
     dateRangeArray[dateRangeIndex].timeSlots = dateRangeArray[dateRangeIndex].timeSlots.map(getGenTime)
   })

   appointmentTemplate.sessions = []
   if(doctor.sessions){
     appointmentTemplate.sessions = doctor.sessions
   }
   appointmentTemplate.appointmentsAvailable = removeEmptySlots(dateRangeArray)

   return appointmentTemplate

 }

function removeEmptySlots(dateRangeArray){
  let appointmentsAvailable = []
  dateRangeArray.forEach(function(dateRange, dateRangeIndex) {

      if(dateRangeArray[dateRangeIndex].timeSlots.length > 0){
        if(appointmentsAvailable.length > 0){
          appointmentsAvailable.push(dateRangeArray[dateRangeIndex])
        }else{
          appointmentsAvailable = [dateRangeArray[dateRangeIndex]]
        }
      }
   })
  return appointmentsAvailable
}

function getFormatedAppointmensArray(appointmentsArray, intervalMinutes){

     let dateAppointmentTime = []
     appointmentsArray.forEach(function(appointment, index) {
       if(appointment.status != "Cancelled"){
            let date = moment(appointment.scheduled_time, "YYYY-MM-DD").format('YYYY-MM-DD')
            let startAppointmentTime = moment(appointment.scheduled_time).format('H:mm')
            let endAppointmentTime = moment(moment(appointment.scheduled_time).add(appointment.duration, 'minutes')).format('H:mm')
            let startH = parseInt(startAppointmentTime.split(":")[0]);
            let startM = parseInt(startAppointmentTime.split(":")[1]);
            let endH = parseInt(endAppointmentTime.split(":")[0]);
            let endM = parseInt(endAppointmentTime.split(":")[1]);
            let startTime = ''
            let endTime = ''
            if(startM >=0 && startM < 30 ){
              startTime = startH+":00"
            }else if (startM >= 30 && startM <= 59 ) {
              startTime = startH+":30"
            }else{
              startTime = startAppointmentTime
            }

            if(endM > 0 && endM < 30 ){
              endTime = endH+":30"
            }else if (endM >= 31 && endM <= 59 ) {
              endH = endH+1
              endTime = endH+":00"
            }else{
              endTime = endAppointmentTime
            }


            let timesInBetweenAppointments = returnTimesInBetween(startTime, endTime, intervalMinutes)

            let startEndArray = {startAppointmentTime, endAppointmentTime, InBetweenAppointment:JSON.stringify(timesInBetweenAppointments)}

            if(dateAppointmentTime[date]){
              dateAppointmentTime[date].push(startEndArray)
            }else{
              dateAppointmentTime[date] = [startEndArray]
            }
        }
      })

  return dateAppointmentTime
}


function getFreeTimeSlots(appointment, timeSlots){

    let timeSlotsArray = [...timeSlots]
    let removeValues = JSON.parse(appointment.InBetweenAppointment)

    removeValues.forEach(function(removeValue, Index){
      var startH = parseInt(removeValue.split(":")[0]);
      var startM = parseInt(removeValue.split(":")[1]);

      timeSlotsArray.forEach(function(timeSlot, index){
        var timeslotH = parseInt(timeSlot.split(":")[0]);
        var timeslotM = parseInt(timeSlot.split(":")[1]);
        // Remove start slots
        if(startH == timeslotH && startM == timeslotM){
          timeSlotsArray.splice(index,1)
        }else if(startH == timeslotH && startM > timeslotM ){
          if(timeSlotsArray[index+1]){
            var timeslotNextH = parseInt(timeSlotsArray[index+1].split(":")[0]);
            var timeslotNextM = parseInt(timeSlotsArray[index+1].split(":")[1]);
            if(startM < timeslotNextM){
              timeSlotsArray.splice(index,1);
            }else if(startM > timeslotNextM){
              timeSlotsArray.splice(index+1,1);
            }
          }
        }
      })
    })

    return timeSlotsArray
}

function getEndTime(duration, startTime, aMPM = ''){
  let date = new Date();
  var startH = parseInt(startTime.split(":")[0]);
  var startM = parseInt(startTime.split(":")[1]);

  date.setHours(startH, startM, 0);

  newDateObj = moment(date).add(duration, 'm').toDate();
  let minutes = (newDateObj.getMinutes() < 10) ? "0"+newDateObj.getMinutes() :  newDateObj.getMinutes()
  let endTime = newDateObj.getHours()+":"+minutes
  if(aMPM != ''){

  }else{
    return endTime
  }

}

 let getGenTime = (timeString) => {
   let timeH = parseInt(timeString.split(":")[0]);
   let timeM = timeString.split(":")[1];
   timeString = (timeH < 10) ? "0"+timeH+":"+timeM : timeString

   let H = +timeString.substr(0, 2);
   let h = (H % 12) || 12;
   let ampm = H < 12 ? " AM" : " PM";
   return timeString = h + timeString.substr(2, 3) + ampm;
 }



function returnTimesInBetween(start, end, interval) {
   var timesInBetween = [];

   var startH = parseInt(start.split(":")[0]);
   var startM = parseInt(start.split(":")[1]);
   var endH = parseInt(end.split(":")[0]);
   var endM = parseInt(end.split(":")[1]);

   if(startM < 30 && endM < 30){
     for(let hour = startH; hour < endH; hour++) {
        timesInBetween.push(moment({ hour }).format('H:mm'));
        timesInBetween.push(
            moment({
                hour,
                minute: 30
            }).format('H:mm')
        );
    }
   }else{
     for (var i = startH; i <= endH; i++) {

       startBeforeH = parseInt(start.split(":")[0]);
       startBeforeM = start.split(":")[1];
       if(i == endH && startBeforeM >=  endM){

       }else{
         timesInBetween.push(i < 10 ? "0" + i + ":"+startBeforeM : start)
       }

       start = getEndTime(interval, start)
       startAfterH = parseInt(start.split(":")[0]);
       if(startBeforeH == startAfterH){
         i--
       }
     }

   }

   return timesInBetween;
 }


 async function getDates(startDate, stopDate, week_days) {
     var dateArray = [];
     var currentDate = moment(startDate);
     var stopDate = moment(stopDate);
     while (currentDate <= stopDate) {
         let dt = moment(currentDate, "YYYY-MM-DD")
         if(week_days.includes(dt.format('dddd'))){
           dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
         }
         currentDate = moment(currentDate).add(1, 'days')
     }
     return dateArray;
 }


 async function changeFormatWeekDays(week_days) {
   let week_days_array = []
   for (i = 0; i < week_days.length; i++) {
        switch(week_days[i]) {
         case 0:
           week_days_array.push("Monday")
           break;
         case 1:
           week_days_array.push("Tuesday")
           break;
         case 2:
           week_days_array.push("Wednesday")
           break;
         case 3:
           week_days_array.push("Thursday")
           break;
         case 4:
           week_days_array.push("Friday")
           break;
         case 5:
           week_days_array.push("Saturday")
           break;
         default:
           week_days_array.push("Sunday")
       }

   }
   return week_days_array

 }

 async function getDoctorAppointmentsTemplateDRChrono(doctorID) {

   let appointmentTemplateURL = 'https://app.drchrono.com/api/appointment_templates?doctor='+doctorID
   let method = "GET"
   let appointmentTemplateArray = {}
   let drchronoDetails = await Drchrono.find();

   //Check if the DRChrono token exists
   if (drchronoDetails && drchronoDetails.length > 0) {

     let access_token = await getAccessToken(drchronoDetails)

     // Params for get doctors list from DRChrono
     var options = {
       'method': method,
       'url': appointmentTemplateURL,
       'headers': {
         'Authorization': 'Bearer ' + access_token
       }
     };

     //Send hit for Get Doctors and update in DB
     await sendRequestForGetDoctorsAppointmentsTemplateAsync(options).then(async jsonData => {
       if (jsonData && jsonData.results) {
         appointmentTemplateArray = jsonData.results[0]
       }
     })
     return appointmentTemplateArray
   }
   return []
 }

 // send hit to get Doctors appointment template from drChrono
 const sendRequestForGetDoctorsAppointmentsTemplateAsync = (options) => {

   return new Promise(resolve => {
     request(options, async function (error, response, body) {
       if (error || body == "" || body == " ") {
         resolve()
       } else {
         jsonData = JSON.parse(body)
         if (jsonData.error) resolve()

         resolve(jsonData);
       }
     })
   })
 }

 async function getDoctorAppointmentsListDRChrono(doctorID = '', dateRange = '', appointURL = null, appointmentArray = []) {

   let appointmentURL = ''
   if(appointURL != null){
     appointmentURL = appointURL;
   }else{
     appointmentURL = 'https://app.drchrono.com/api/appointments?page_size=1000&doctor='+doctorID+'&date_range='+dateRange
   }

   let method = "GET"
   let drchronoDetails = await Drchrono.find();

   //Check if the DRChrono token exists
   if (drchronoDetails && drchronoDetails.length > 0) {

     let access_token = await getAccessToken(drchronoDetails)

     // Params for get doctors list from DRChrono
     var options = {
       'method': method,
       'url': appointmentURL,
       'headers': {
         'Authorization': 'Bearer ' + access_token
       }
     };

     //Send hit for Get Doctors and update in DB
     await sendRequestForGetDoctorsAppointmentsAsync(options).then(async jsonData => {
       if (jsonData && jsonData.results) {
         let results = jsonData.results
         Object.keys(results).forEach(result => {
           appointmentArray.push(results[result])
         })
         console.log("I am Here")
         if(jsonData.next != null){
           await getDoctorAppointmentsListDRChrono('','',jsonData.next,appointmentArray)
         }
       }
     })
     return appointmentArray
   }
   return []
 }


 // send hit to get Doctors appointment List from drChrono
 const sendRequestForGetDoctorsAppointmentsAsync = (options) => {

   return new Promise(resolve => {
     request(options, async function (error, response, body) {
       if (error || body == "" || body == " ") {
         resolve()
       } else {
         jsonData = JSON.parse(body)
         if (jsonData.error) resolve()

         resolve(jsonData);
       }
     })
   })
 }


 /**
  * API to create new doctors from web
  */
 async function addDoctorsData(userParam, profile_picture){

   let userCheck = await Doctor.findOne({ email: userParam.email });

   if (userCheck && userCheck.email == userParam.email) {
     throw 'Email "' + userParam.email + '" is already taken';
   }

   if(profile_picture){
     userParam.profile_picture = profile_picture.replace('public/', '')
   }

   if(userParam.clinicID && userParam.clinicID != ""){
     const clinic = await Clinic.findById(userParam.clinicID)
    userParam.clinicName = clinic.title
  }else{
    userParam.clinicName = undefined
    userParam.clinicID = undefined
  }

   const doctor = new Doctor(userParam);
   doctor.save()

   return {status:'success', message:'Doctor Saved successfully'}
 }


 /**
  * API to update doctors from web
  */
 async function updateDoctorsDataForm(userParam, profile_picture){

   // let userCheck = await Doctor.findOne({ email: userParam.email });
   let userCheck = await Doctor.findOne({$and : [
                                                   { email:userParam.email},
                                                   { _id: { $nin :mongoose.Types.ObjectId(userParam.docID)  } }
                                                 ]}
                                               );

   if (userCheck && userCheck.email == userParam.email) {
     throw 'Email "' + userParam.email + '" is already taken';
   }

   if(profile_picture && profile_picture != ""){
     userParam.profile_picture = profile_picture.replace('public/', '')
   }else{
     delete userParam.profile_picture
   }

   const doctor = await Doctor.findById(userParam.docID)
   delete userParam.docID
   if(userParam.clinicID && userParam.clinicID != ""){
     const clinic = await Clinic.findById(userParam.clinicID)
    userParam.clinicName = clinic.title
   }else{
     userParam.clinicName = undefined
     userParam.clinicID = undefined
   }

   Object.assign(doctor, userParam);
   await doctor.save()

   return {status:'success', message:'Doctor updated successfully'}
 }


 /**
  * API to create doctors sessions from web
  */
 async function addSessions(userParam, docID){

   const doctor = await Doctor.findById(docID)
   let sessions = []
   let sessionParams = {}
   if(doctor.sessions && doctor.sessions.length > 0){
     sessions = doctor.sessions
     sessions.push(userParam)
     sessionParams.sessions = sessions
   }else{
     sessions.push(userParam)
     sessionParams.sessions = sessions
   }

   Object.assign(doctor, sessionParams);
   await doctor.save()

   return {status:'success', message:'Sessions created successfully'}
 }

 /**
  * API to update doctors sessions from web
  */
 async function updateSessions(userParam, docID){

   const doctor = await Doctor.findById(docID)

   Object.assign(doctor, userParam);
   await doctor.save()

   return {status:'success', message:'Sessions updated successfully'}
 }

 /**
  * API to update doctors Template from web
  */
 async function updateTemplate(userParam, docID){

   const doctor = await Doctor.findById(docID)

   let templateID = userParam.tempId
   delete userParam.tempId
   let updateTemplate = await updateDoctorAppointmentsTemplateDRChrono(userParam, templateID)
   if(updateTemplate){
     throw "Error while updating template"
   }else{
     return {status:'success', message:'Template updated successfully'}
   }


 }

 async function updateDoctorAppointmentsTemplateDRChrono(userParam, appointmentTemplateID) {

   let appointmentTemplateURL = 'https://app.drchrono.com/api/appointment_templates/'+appointmentTemplateID
   let method = "PUT"

   let drchronoDetails = await Drchrono.find();

   //Check if the DRChrono token exists
   if (drchronoDetails && drchronoDetails.length > 0) {

     let access_token = await getAccessToken(drchronoDetails)
     let bodyParams = {
       scheduled_time:userParam.scheTime,
       week_days: userParam.week_days_array,
       duration: userParam.tempDuration,
       office:userParam.office,
       exam_room:userParam.exam_room,
       profile:userParam.profile
     }
     // Params for get doctors list from DRChrono
     var options = {
       'method': method,
       'url': appointmentTemplateURL,
       'headers': {
         'Authorization': 'Bearer ' + access_token
       },
       form: bodyParams,
     };
     console.log(options)
     //Send hit for update in appointment template
     await sendRequestForGetDoctorsAppointmentsTemplateAsync(options).then(async jsonData => {
       console.log(jsonData)
       return jsonData
     })
   }
 }
