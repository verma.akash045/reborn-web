﻿const express = require('express');
const router = express.Router();
const drchronoService = require('./drchrono.service');

const request = require('request');

// routes
router.get('/generate', sendRequestForCode);
router.get('/', saveTokenInDB);
router.get('/iframe', getPatientData)

module.exports = router;

// Function for save DRChrono in our DB
function saveTokenInDB(req, res, next) {
  console.log(' in token save')
  drchronoService.create(req.query.code)
    .then(drchrono => res.json(drchrono))
    .catch(err => next(err));
}

// Function for get request URL for the auth code for DRChrono
function sendRequestForCode(req, response, next) {
  let authURL = 'https://drchrono.com/o/authorize/?redirect_uri=http://localhost:4000/drchrono&response_type=code&client_id=9ZdFolxjMgEGrMibm393ZhjiNPit3DlO1JXRy8RH&scope=tasks labs:write billing:read patients:read user:write messages:write billing:patient-payment:write user tasks:write patients:summary:write calendar user:read billing:patient-payment tasks:read billing:patient-payment:read patients:summary:read labs billing labs:read settings:read billing:write messages calendar:write clinical:read patients messages:read clinical:write clinical settings:write settings patients:summary patients:write calendar:read';
  request(authURL, { json: true }, (err, res, body) => {
    if (err) { console.log(err); return }

    return response.redirect('/drchrono');
  });
}

// Function for get paitent data according to paitent ID for iFrame
function getPatientData(req, res, next) {
  drchronoService.getPatient(req.query.patient_id)
    .then(user => user ? res.render('iframe', {page:'Upcomming Appointments', user:user}) : res.render('iframe', {page:'Upcomming Appointments', user:"user not found"} ) )
    .catch(err => next(err));
}
