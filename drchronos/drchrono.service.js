const config = require('config.json');
﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const request = require('request');
var moment = require('moment');
const Drchrono = db.Drchrono;
const User = db.User;
const Appointment = db.Appointment;

module.exports = {
    create, getPatient
};

/**
 * API for create auth_token for code and save in our DB
 */
async function create(code) {

  if(!code){
    throw "Code is required"
  }
  let drchrono = new Drchrono();

  let bodyParams = {
    code: code,
    grant_type: config.drchrono_grant_type,
    redirect_uri: config.drchrono_redirect_uri,
    client_id: config.drchrono_client_id,
    client_secret: config.drchrono_client_secret,
  }

  var options = {
  'method': 'POST',
  'url': 'https://drchrono.com/o/token/',
  'headers': {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  form: bodyParams,
};

return await parseJsonAsync(options).then(async jsonData => {
  if(jsonData){

    await Drchrono.remove({}).then()
    let date_ob = new Date();
    var newDate = new Date(date_ob.getTime() + (1000 * jsonData.expires_in))
    drchrono.access_token = jsonData.access_token
    drchrono.expires_in = formatDate(newDate)
    drchrono.refresh_token = jsonData.refresh_token

    // save DRchrono Token
    await drchrono.save()

    if (drchrono && drchrono.id){
      return {status:"success", message:"Drchrono token created successfully"}
    }
  }
})

}

// Function for format date after adding expirt time
function formatDate(date){
    return ('{0}-{1}-{3} {4}:{5}:{6}').replace('{0}', date.getFullYear()).replace('{1}', date.getMonth() + 1).replace('{3}', date.getDate()).replace('{4}', date.getHours()).replace('{5}', date.getMinutes()).replace('{6}', date.getSeconds())
}


// Function to send request on DRChrono for auth_code
const parseJsonAsync = (options) => {
  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error) {
        resolve()
      }
      jsonData = JSON.parse(body)
      if(jsonData.error) resolve()

      resolve(jsonData);
    })
  })
}

/**
 * API for get patient data according to the patitentID for iFrame page
 */
async function getPatient(patientID) {
  if (!patientID) throw 'Patient Id is required';

  let patient = await User.findOne({ patientID }).select(['-hash', '-resetToken','-__v']).populate('diseaseCondition')
  if (!patient) throw 'Patient not Found';


  let upcommingAppointments = await getAppointments(patientID)
  if(upcommingAppointments.length < 1){
    throw "No Upcomming appointment yet"
  }

  patient = patient.toObject()
  let patientAge = getAge( moment(patient.dob).format('YYYY/MM/DD') )
  patient.age = (patientAge <= 0 ) ? "Less then 1 year" : patientAge+" years old"
  let firstName = ''
  let lastName = ''
  if(patient.firstName && patient.firstName != ""){
    firstName = user.firstName
  }
  if(patient.lastName && patient.lastName != ""){
    lastName = user.lastName
  }
  patient.name = (firstName != '' || lastName != '' ) ? firstName+" "+lastName : patient.name
  patient.appointments = upcommingAppointments

  return patient
}

// function to calulate age
function getAge(dob) {
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

// function to get upcomming appointments
async function getAppointments(drChronoPatientID) {
  let currentDateTime = moment().format('YYYY-MM-DD HH:mm')
  let appointmentsArray = []
  let appointments = await Appointment.find( {$and : [
                                            { scheduled_time: { $gt: currentDateTime } },
                                            { patientID:drChronoPatientID},
                                            { status: { $nin : ["Cancelled", "Complete"] } }
                                          ]
                                        }, {doctorID:1,patientID:1,drChronoAppintmentID:1,notes:1,exam_room:1,office:1,duration:1,scheduled_time:1,status:1,reason:1,zoomJoinURL:1, zoomStartURL:1, _id:1})
                                        .sort({scheduled_time: 1})

    appointments.map( appointment => {
      let appointmentObject = appointment.toObject();
      appointmentObject.scheduled_time = moment(appointmentObject.scheduled_time).format('LLLL')
      if(appointmentsArray.length > 0){
        appointmentsArray.push(appointmentObject)
      }else{
        appointmentsArray = [appointmentObject]
      }
    })

    return appointmentsArray;
}
