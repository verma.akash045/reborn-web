require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
var path = require('path');
const bodyParser = require('body-parser');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');

//setup SWAGGER
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');


app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/', require('./users/users.controller'));
app.use('/diseaseConditions', require('./diseaseConditions/diseaseConditions.controller'));
app.use('/goals', require('./goals/goals.controller'));
app.use('/drchrono', require('./drchronos/drchronos.controller'));
app.use('/doctors', require('./doctors/doctors.controller'));
app.use('/appointments', require('./appointments/appointments.controller'));
app.use('/clinics', require('./clinics/clinics.controller'));
app.use('/communication', require('./emailTemplates/emailTemplates.controller'));
app.use('/feedbacks', require('./feedbacks/feedbacks.controller'));
app.use('/members', require('./patients/patients.controller'));
app.use('/medicines', require('./medicines/medicines.controller'));
// global error handler
app.use(errorHandler);

// start server
const port = 4000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
