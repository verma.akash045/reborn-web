﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const Feedback = db.Feedback;

module.exports = {
    getFeedback,
    createFeedback,
    updateFeedback
};


/**
 * API for get feedback for appointment
 */
async function getFeedback(appointmentID) {

  let feedback = await Feedback.find({ appointmentID:appointmentID }).populate( {path:'user', select:["firstName", "lastName", "name", "email" ]} )
  if(feedback){
    return feedback
  }

}

/**
 * API for create feedback according to appointment
 */
async function createFeedback(bodyParams, userID, appointmentID) {

  bodyParams.appointmentID = appointmentID
  bodyParams.user = userID

  const feedback = new Feedback(bodyParams)

  await feedback.save()
  if(feedback._id){
    let createdFeedback = await Feedback.findById(feedback._id).populate( {path:'user', select:["firstName", "lastName", "name", "email" ]} )
    return {status:200, message:"Feedback submitted successfully", feedback:createdFeedback}
  }

}

/**
 * API for update feedback
 */
async function updateFeedback(bodyParams, userID, feedbackID) {

  let feedback = await Feedback.findOne({$and: [{ _id: mongoose.Types.ObjectId(feedbackID)},{ user:userID}] })
  if(!feedback || !feedback._id){
    throw "Feedback is not available or it is not you feedback"
  }

  Object.assign(feedback, bodyParams);
  await feedback.save()
  let updatedFeedback = await Feedback.findById(feedback._id).populate( {path:'user', select:["firstName", "lastName", "name", "email" ]} )
  return {status:200, message:"Feedback Updated successfully", feedback:updatedFeedback}
}
