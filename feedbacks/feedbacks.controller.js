﻿const express = require('express');
const router = express.Router();
const feedback = require('./feedback.service');

// routes
// Get
router.get('/:appointmentID', getFeedback);
// POST
router.post('/create/:appointmentID', createFeedback);
// PATCH
router.patch('/update/:feedbackID', updateFeedback);

module.exports = router;

// Function for get forgot password template
function getFeedback(req, res, next) {
    feedback.getFeedback(req.params.appointmentID)
    .then(feedback => feedback ? res.json(feedback) : res.json([]) )
    .catch(err => next(err));
}

// Function to create template
function createFeedback(req, res, next) {
    feedback.createFeedback(req.body, req.user.sub, req.params.appointmentID)
    .then(feedback => feedback ? res.json(feedback) : res.status(400).json({ status: "error", message: "Error while creating feedback" }) )
    .catch(err => next(err));
}

// Function to create template
function updateFeedback(req, res, next) {
    feedback.updateFeedback(req.body, req.user.sub, req.params.feedbackID)
    .then(feedback => feedback ? res.json(feedback) : res.status(400).json({ status: "error", message: "Error while updating feedback" }) )
    .catch(err => next(err));
}
