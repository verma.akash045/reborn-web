const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    appointmentID: { type: String },
    user: { type: String, ref: 'User' },
    feedback_call: { type: Number },
    feedback_overall: { type: Number },
},{
  timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Feedback', schema);
