﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const Goal = db.Goal;

module.exports = {
    getAll,
    create,
    getById
};

/**
 * API for get all data of goals
 */
async function getAll() {
    return await Goal.find();
}

/**
 * API for create goals and save into our DB
 */
async function create(goalParam) {
  if (!goalParam.image) throw 'Image is required'

  if (!goalParam.title) throw 'Title is required'

  const goal = new Goal(goalParam);

  // save goal
  await goal.save();
  if (goal && goal.id){
    return {status:"success", message:"goal created successfully", goal:goal}
  }
}

/**
 * API for get goal data by goalID
 */
async function getById(id) {
    let goalID = mongoose.Types.ObjectId(id);
    return await Goal.findById(goalID);
}
