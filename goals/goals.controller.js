﻿const express = require('express');
const router = express.Router();
const goalService = require('./goal.service');

// routes
router.get('/', getAll);
router.post('/createGoal', createGoal);
router.get('/:id', getById);

module.exports = router;

// Function for get all goals
function getAll(req, res, next) {
    goalService.getAll()
        .then(goals => res.json(goals))
        .catch(err => next(err));
}

// Function for create goals
function createGoal(req, res, next) {
    goalService.create(req.body)
        .then(goal => goal ? res.json(goal) : res.status(400).json({ status: "error", message: 'Error while creating goal' }))
        .catch(err => next(err));
}

// Function to get goal data by ID
function getById(req, res, next) {
    goalService.getById(req.params.id)
        .then(goal => goal ? res.json(goal) : res.status(404).json({ status: "error", message: 'Goals not found' }))
        .catch(err => next(err));
}
