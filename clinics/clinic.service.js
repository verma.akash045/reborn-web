﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const Clinic = db.Clinic;
const Doctor = db.Doctor;

module.exports = {
    create,
    getAll,
    deleteClinicData,
    update,
    getSearchData,
    getClinicByID
};

/**
 * API for get all data of goals
 */
async function getAll() {
  let clinics = await Clinic.find().sort({$natural: -1});
  let clinicArray = []

  await Promise.all(
    clinics.map(async clinic => {
      clinic = clinic.toObject()
      let clinicDoctorsCount = await Doctor.find({clinicID:clinic._id}).count()
      clinic.doctorsCount = clinicDoctorsCount
      clinicArray.push(clinic)
    })
  );

  return clinicArray
}


/**
 * API for get all data of goals
 */
async function getClinicByID(clinicID) {

    let clinic = await Clinic.findById(clinicID);

    let clinicDoctors = await Doctor.find({clinicID:clinicID})
    
    return {clinic, clinicDoctors}


}

/**
 * API for create clinics and save into our DB
 */
async function create(clinicParam) {

  const clinic = new Clinic(clinicParam)
  await clinic.save()

  return {status: "success", message: "Clinic created successfully"}
}

/**
 * API for update clinics and save into our DB
 */
async function update(clinicParam) {

  const clinic = await Clinic.findById(clinicParam.clinicID)
  delete clinicParam.clinicID

  Object.assign(clinic, clinicParam);
  clinic.save()

  return {status: "success", message: "Clinic updated successfully"}
}

/**
 * API to remove doctor
 */
async function deleteClinicData(clinicId){
    let doctors = await Doctor.find({clinicID:clinicId})
    let param = {
      clinicName: undefined,
      clinicID: undefined
    }
    doctors.forEach(async function(doctor ,Index){
      const doc = await Doctor.findById(doctor._id)
      Object.assign(doc, param);
      doc.save()
    })

    await Clinic.findByIdAndRemove(clinicId);

    return {status:"success", message:"Clinic removed"}
}

/**
 * API to search doctors and clinics
 */
async function getSearchData(searchData){
    let searchResults = []
    let results = await Doctor.find({ $or: [ { first_name: new RegExp(searchData, 'i') }, { last_name: new RegExp(searchData, 'i') }, {clinicName: new RegExp(searchData, 'i') } ] })
    results.forEach(function (result, index){
      let clinicName = (result.clinicName) ? "- "+result.clinicName : ""
      let objectParams = {
        resultTitle: `${result.first_name} ${result.last_name} ${clinicName}`,
        clinicName: (result.clinicName) ? `${result.clinicName}` : "",
        doctorName: `${result.first_name} ${result.last_name}`,
        doctorID: `${result._id}`,
        clinicID: (result.clinicID) ? result.clinicID : "",
        profilePicture: result.profile_picture
      }
      if(searchResults.length > 0){
        searchResults.push(objectParams)
      }else{
        searchResults = [objectParams]
      }

    })

    return searchResults
}
