﻿const express = require('express');
let session = require('express-session')
const router = express.Router();
const clinicService = require('./clinic.service');

// routes
router.get('/', getAll);
router.get('/search', getSearchData);
router.get('/details/:clinicID', getClinicByID);

// POST
router.post('/create', createClinic);
router.post('/update', updateClinic);

// DELETE
router.delete('/delete', deleteClinicData);

module.exports = router;

// Function for get all clinic
function getAll(req, res, next) {
    if(req.session.token){
     clinicService.getAll()
    .then(user => user ? res.render('clinics/clinics', {page:'Add Clinics', clinics:user,  error:""}) : res.render('clinics/clinics', {page:'Add Clinics', clinics:"Clinics not found", error:""} ) )
    .catch(err => res.render('clinics/clinics', {page:'Add Clinics', clinics:[],  error:err}));
    }else{
        res.redirect('/adminLogin')
    }
}

// Function for get all clinic
function getClinicByID(req, res, next) {
    if(req.session.token){
        clinicService.getClinicByID(req.params.clinicID)
    .then(user => user ? res.render('clinics/clinicDetails', {page:'Clinics Details', clinic:user.clinic, doctors:user.clinicDoctors, error:""}) : res.render('clinics/clinicDetails', {page:'Clinics Details', clinic:"Clinic not found", doctors:[], error:""} ) )
    .catch(err => res.render('clinics/clinicDetails', {page:'Clinics Details', clinic:{}, doctors:[], error:err}));
    }else{
        res.redirect('/adminLogin')
    }
}

// Function for create Clinic
function createClinic(req, res, next) {
    clinicService.create(req.body)
        .then(user => user ? res.json(user) : res.json([]) )
        .catch(err => next(err) );
}

// Function for update Clinic
function updateClinic(req, res, next) {
    clinicService.update(req.body)
        .then(user => user ? res.json(user) : res.json([]) )
        .catch(err => next(err) );
}


// Function for get deleteDoctor
function deleteClinicData(req, res, next) {
  clinicService.deleteClinicData(req.body.clinicID)
    .then(user => user ? res.json(user) : res.json([]) )
    .catch(err => next(err) );
}

// Function for get deleteDoctor
function getSearchData(req, res, next) {
  clinicService.getSearchData(req.query.search)
    .then(user => user ? res.json(user) : res.json([]) )
    .catch(err => next(err) );
}
