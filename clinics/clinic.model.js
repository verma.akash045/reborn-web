const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    DRChronoID: { type: String },
    title: { type: String, required: true },
    address: { type: String },
    contact: { type: String },
    contactEmail: { type: String },
},{
  timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Clinic', schema);
