﻿const express = require('express');
const session = require('express-session') 
const router = express.Router();
const userService = require('./user.service');

// routes
router.use(session({secret:'secret', resave:false, cookie: {maxAge: new Date( Date.now() + 168 * 360000)}}))
//POST
router.post('/login', authenticate)
router.post('/signUp', register)
router.post('/adminLogin', authenticateAdmin)
router.post('/signUpPhoneNumber', checkExistingPhoneNumber)
router.post('/verifyPhoneNumber', verifyPhoneNumber)
router.post('/signUpEmail', verifyEmail)
router.post('/setNewPasswordWithToken', setNewPasswordWithToken)
router.post('/forgotPassword', forgotPassword)
router.post('/reset', resetUserPassword);

//PUT
router.patch('/user', updateUser);
router.patch('/user/:id', update);

//GET
router.get('/users', getAll);
router.get('/adminLogin', getAdminLogin);
router.get('/adminLogout', adminLogout)
router.get('/user/current', getCurrent);
router.get('/user/:id', getById);
router.get('/reset', resetPassword);

//DELETE
router.delete('/user/:id', _delete);

module.exports = router;

// Function for authenticate user's details
function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Oops, wrong credentials, please try again' }))
        .catch(err => next(err));
}

// Function for authenticate admin 's details
function authenticateAdmin(req, res, next) {
    userService.authenticateAdmin(req.body)
        .then(user => {
            if (user) {
                sess = req.session
                sess.token = user.token
                res.json(user)
            } else {
                res.status(400).json({ status: "error", message: 'Oops, wrong credentials, please try again' })
            }
        })
        .catch(err => next(err));
}


// Function to logout admin
function adminLogout(req, res, next) {
    console.log("logout")
    req.session.destroy((err) =>{
        if(err){
            res.send(err)
        }else{
            res.redirect('/adminLogin')
        }
    })
}

function getAdminLogin(req, res, next) {
    if(req.session.token){
        res.redirect('/clinics')
    }else{
        res.render('adminLogin', {page:'Admin Login',  error:""})
    }    
}


// Function for register user's details
function register(req, res, next) {
    console.log(req.body)
    userService.create(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Error while registring user' }))
        .catch(err => next(err));
}

// Function for get all users
function getAll(req, res, next) {
    console.log(req.body, req.params)
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

// Function for get login user details
function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.status(404).json({ status: "error", message: 'Disease condition not found' }))
        .catch(err => next(err));
}

// Function for get users details by ID
function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.status(404).json({ status: "error", message: 'Disease condition not found' }))
        .catch(err => next(err));
}

// Function for update users detail by ID
function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Error while updating user' }))
        .catch(err => next(err));
}

// Function for update user detail's by token
function updateUser(req, res, next) {
    userService.update(req.user.sub, req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Error while updating user' }))
        .catch(err => next(err));
}

// Function for delete user's from our DB
function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({status:"success", message:"User deleted successfully"}))
        .catch(err => next(err));
}

// Function for forgot password via email or phone
function forgotPassword(req, res, next) {

    if(req.body.email){
      let baseURL = req.protocol+"://"+req.headers.host
      userService.getUserByEmail(req.body.email, baseURL)
          .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Email not found' }))
          .catch(err => next(err));
    }else if(req.body.phone){
      userService.getUserByPhone(req.body.phone)
          .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Phone not found' }))
          .catch(err => next(err));
    }

}

// Function set new password with token with api
function setNewPasswordWithToken(req, res, next) {
    userService.setNewPasswordWithToken(req.query, req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: 'Invalid or expired link' }))
        .catch(err => next(err));
}

// Function for reset user's password from webpage
function resetUserPassword(req, res, next) {
    userService.setNewPasswordWithToken(req.query, req.body)
        .then(user => user ? res.render('resetPassword', {page:'Set Password', user:user, body:req.body, error:""}) : res.render('resetPassword', {page:'Set Password', user:"Invalid or expired link", error:"", body:{}} ) )
        .catch(err => res.render('resetPassword', {page:'Set Password', body:req.body, user:"",error:err}));
}

// Function for check token and render web page for forgot page
function resetPassword(req, res, next) {
    userService.resetCheckTokenAndData(req.query.token)
        .then(user => user ? res.render('resetPassword', {page:'Set Password', user:user, error:"", body:{}}) : res.render('resetPassword', {page:'Set Password', user:"Invalid or expired link", error:"", body:{}} ) )
        .catch(err => res.render('resetPassword', {page:'Set Password',body:req.body, user:"",error:err}));
}

// Function for check if phoneNumber exists
function checkExistingPhoneNumber(req, res, next) {
    userService.checkExistingPhoneNumber(req.body.phone)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: "Phone Number already exists" }))
        .catch(err => next(err));
}

// Function for verify phone number
function verifyPhoneNumber(req, res, next) {
    userService.verifyPhoneNumber(req.body.phone)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: "Phone number not found." }))
        .catch(err => next(err));
}

// Function for verify email
function verifyEmail(req, res, next) {
    userService.verifyEmail(req.body.email)
        .then(user => user ? res.json(user) : res.status(400).json({ status: "error", message: "Email already exists" }))
        .catch(err => next(err));
}
