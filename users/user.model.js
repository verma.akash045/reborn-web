const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  email: { type: String, required: true },
  hash: { type: String, required: true },
  isAdmin: { type: Boolean, default: false },
  name: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  givenName: { type: String },
  dob: { type: Date },
  gender: { type: String },
  resetToken: { type: String },
  phone: { type: Number },
  isVerifiedPhone: { type: Boolean, default: false },
  weight: { type: Number },
  patientID: { type: String },
  step: { type: Number },
  deviceId: { type: String },
  firebaseToken: { type: String },
  diseaseCondition: [{ type: String, ref: 'diseaseCondition' }],
  goals: { type: Array, "default": [] },
  doctorID: { type: String },
  clinicID: { type: String },
  shippingAddress: { type: String },
  status: { type: String, default:"Active" },
}, {
    timestamps: true
  });

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);
