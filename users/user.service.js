﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const request = require('request');
var moment = require('moment');
var mongoose = require('mongoose');
const User = db.User;
const Goal = db.Goal;
const Drchrono = db.Drchrono;
const DiseaseCondition = db.DiseaseCondition;
const EmailTemplate = db.EmailTemplate;
const mailer = require('../_helpers/nodemailer')
const mailgu = require('../_helpers/mailgun')

module.exports = {
  authenticate,
  authenticateAdmin,
  getAll,
  getById,
  create,
  update,
  delete: _delete,
  getUserByEmail,
  verifyPhoneNumber,
  verifyEmail,
  setNewPasswordWithToken,
  resetCheckTokenAndData,
  checkExistingPhoneNumber,
  getUserByPhone
};

/**
 * API for authenticate user's details and send user details and token in response
 */
async function authenticate({ email, password }) {
  let user = await User.findOne({ email }).populate('diseaseCondition');
  if (user && bcrypt.compareSync(password, user.hash)) {
    user = await getGoals(user);
    const { hash, ...userWithoutHash } = user.toObject();
    const token = jwt.sign({ sub: user.id }, config.secret);
    return {
      status: "success",
      ...userWithoutHash,
      token,
      step: user.step < 7 ? user.step : 0
    };
  }
}


async function authenticateAdmin({ email, password }) {
  let user = await User.findOne({ email }) 
  if (user && bcrypt.compareSync(password, user.hash) && user.isAdmin) {
    const token = jwt.sign({ sub: user.id }, config.secret);
    return {
      status: "success",
      token
    };
  }
}

/**
 * API for get all users from DB
 */
async function getAll() {
  return await User.find().select('-hash').populate('diseaseCondition');
}

/**
 * API for get user details by userID
 */
async function getById(id) {
  let user = await User.findById(id).select('-hash').populate('diseaseCondition');
  let newUser = await getGoals(user);
  return newUser
}

/**
 * API for register user in our DB
 */
async function create(userParam) {
  // validate
  if (!userParam.email) {
    throw 'Email is required'
  }
  userParam.email = userParam.email.toLowerCase()

  let userCheck = await User.findOne({ email: userParam.email }).select('-hash');

  if (userCheck && userCheck.email == userParam.email) {
    throw 'Email "' + userParam.email + '" is already taken';
  }
  const user = new User(userParam);
  if (!userParam.password) {
    throw 'Password is required'
  }
  var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
  if (!passwordRegex.test(userParam.password)) {
    throw 'Password must be greater than or equals to 8 characters, Contains 1 uppercase, 1 lowercase and 1 number'
  }

  // Name is not used yet so commented
  // if (!userParam.name) {
  //   throw 'name is required'
  // }

  // removed validation for now will use in future
  // if (!userParam.phone) {
  //   throw 'phone number is required'
  // }

  if (!userParam.deviceId) {
    throw 'Device Id is required'
  }

  if (!userParam.firebaseToken) {
    throw 'Firebase token is required'
  }

  // hash password
  user.hash = bcrypt.hashSync(userParam.password, 10);
  // default step
  user.step = 0
  // save user
  await user.save();
  if (user && user.id) {
    const token = jwt.sign({ sub: user.id }, config.secret);
    return { status: "success", message: "User register successfully", token: token }
  }
}

/**
 * API for update user's details in our DB and DRChrono if step is greater than 5
 */
async function update(id, userParam) {
  if (id.match(/^[0-9a-fA-F]{24}$/)) {

    const user = await User.findById(id).select('-hash');
    userParam = await clearUserObject(userParam)

    // validate
    if (!user) throw 'User not found';
    if(userParam.email){
      userParam.email = userParam.email.toLowerCase()
    }
    if (userParam.email && user.email !== userParam.email && await User.findOne({ email: userParam.email })) {
      throw 'Email "' + userParam.email + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
      var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
      if (!passwordRegex.test(userParam.password)) {
        throw 'Password must be greater than or equals to 8 characters, Contains 1 uppercase, 1 lowercase and 1 number'
      }

      userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }
    let firstName = ''
    let lastName = ''

    if (userParam.firstName) {
        firstName = userParam.firstName
    }

    if (userParam.lastName) {
        lastName = " "+userParam.lastName
    }
    // Handeled name on our side as we are using name in various places
    userParam.name = firstName+""+lastName

    if (userParam.dob) {
      // let dob = moment(userParam.dob)
      let dob = new Date(userParam.dob).getTime();
      userParam.dob = moment(dob).format('YYYY-MM-DD');
    }

    if(userParam.diseaseCondition && userParam.diseaseCondition.length > 0){
      let diseaseConditions = userParam.diseaseCondition
      userParam.diseaseCondition = []
      Object.keys(diseaseConditions).forEach(disease => {
          if(diseaseConditions[disease].id){
            userParam.diseaseCondition.push(diseaseConditions[disease].id)
          }
      });
    }

    if(userParam.otherDiseaseCondition && userParam.otherDiseaseCondition != "" ){
      let otherDisease = await checkOtherCondition(userParam.otherDiseaseCondition, id)
      if(otherDisease.otherCondition != "found"){
        let otherDiseaseConditionID = await createOtherDiseaseCondition(userParam.otherDiseaseCondition)
        delete userParam.otherDiseaseCondition
        if(userParam.diseaseCondition && Array.isArray(userParam.diseaseCondition)){
          userParam.diseaseCondition.push(otherDiseaseConditionID)
        }
      }else{
        delete userParam.otherDiseaseCondition
        userParam.diseaseCondition.push(otherDisease.otherConditionID)
      }
    }

    //copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
    if(userParam && userParam.step && userParam.step > 4) {
      await createORUpdateUserOnDRChrono(id);
    }

    let newUser = await User.findById(id).select('-hash').populate('diseaseCondition');
    newUser = await getGoals(newUser);
    return {
      status: "success",
      user: newUser,
      message: "User updated successfully"
    };
  }
}

// Function to check other disease condition
async function checkOtherCondition(otherDiseaseCondition, id){
  let user = await User.findById(id).select('-hash').populate('diseaseCondition');
  user = user.toObject()
  let otherCondition = "not Found"
  let otherConditionID = ""
  Object.keys(user.diseaseCondition).forEach(disease => {
      if(user.diseaseCondition[disease].title == otherDiseaseCondition){
        otherCondition = "found"
        otherConditionID = user.diseaseCondition[disease]._id
      }
  });

  return {otherCondition, otherConditionID:otherConditionID}
}

// Function to remove null and blank values from user params
async function clearUserObject(userParams){
  for (var propName in userParams) {
    if (userParams[propName] === null || userParams[propName] === undefined || userParams[propName] == "") {
      delete userParams[propName];
    }
  }

  return userParams;
}

// Function to create a disease condition with type other in user params
async function createOtherDiseaseCondition(title) {
  let diseaseConditionParam = {
      	"title" : title,
      	"type":"Other"
      }
  let diseaseConditionID = "";
  let diseaseCondition = new DiseaseCondition(diseaseConditionParam)
  await diseaseCondition.save()
  diseaseConditionID = diseaseCondition.id

  return diseaseConditionID;
}

// Create or update user's details on DRChrono
async function createORUpdateUserOnDRChrono(usersID) {

  let user = await User.findById(usersID).select('-hash')
  let patientsURL = 'https://app.drchrono.com/api/patients'
  let method = "POST"
  if (user.patientID && user.patientID != "") {

    patientsURL = 'https://app.drchrono.com/api/patients/' + user.patientID
    method = "PUT"
  }

  let drchronoDetails = await Drchrono.find();

  //Check if the DRChrono token exists
  if (drchronoDetails && drchronoDetails.length > 0) {

    let tokenExpireTimestamp = Date.parse(drchronoDetails[0].expires_in)
    let currentDateTimestamp = Date.now()
    let access_token = "";
    //check if the token is expired
    if (tokenExpireTimestamp < currentDateTimestamp) {

      let bodyParams = {
        refresh_token: drchronoDetails[0].refresh_token,
        grant_type: 'refresh_token',
        client_id: config.drchrono_client_id,
        client_secret: config.drchrono_client_secret,
      }
      var options = {
        'method': 'POST',
        'url': 'https://drchrono.com/o/token/',
        'headers': {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        form: bodyParams,
      };
      //get new toket and delete expired token
      await getRefreshTokenAsync(options).then(async jsonData => {
        if (jsonData) {
          await Drchrono.remove({}).then()

          let date_ob = new Date();
          var newDate = new Date(date_ob.getTime() + (1000 * jsonData.expires_in))
          access_token = jsonData.access_token

          let refresh_token = {
              	"access_token" : access_token,
              	"expires_in":formatDate(newDate),
                "refresh_token":jsonData.refresh_token
              }

          let drchrono = new Drchrono(refresh_token);
          // save DRchrono Token
          await drchrono.save()
        }
      })
    } else {
      access_token = drchronoDetails[0].access_token
    }
    //Params for Create or Update patient on DRChrono
    var options = {
      'method': method,
      'url': patientsURL,
      'headers': {
        'Authorization': 'Bearer ' + access_token,
        'Content-Type': 'application/json',
      },
      form: {
        "doctor": config.drchrono_doctorID,
        "email": user.email,
        "first_name": user.name.substr(0, user.name.indexOf(' ')),
        "last_name": user.name.substr(user.name.indexOf(' ') + 1),
        "gender": (user.gender) ? capitalizeFirstLetter(user.gender) : "",
        "weight": (user.weight) ? user.weight : "",
        "date_of_birth": (user.dob) ? moment(user.dob).format('YYYY-MM-DD') : ""
      },
    };
    //Send hit for Create or Update patient on DRChrono
    await sendRequestForPatientsAsync(options).then(async jsonData => {

      if (jsonData && jsonData.id) {
        let userParam = {
          "patientID": jsonData.id
        }
        Object.assign(user, userParam);
        await user.save()
      }
    })

  }

}

// promise for send hit to DRChrono to create or update patient
const sendRequestForPatientsAsync = (options) => {

  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error || body == "" || body == " ") {
        resolve()
      } else {
        jsonData = JSON.parse(body)
        if (jsonData.error) resolve()

        resolve(jsonData);
      }
    })
  })
}

// Send hit to DRChrono for get auth_code by referes token
const getRefreshTokenAsync = (options) => {
  return new Promise(resolve => {
    request(options, async function (error, response, body) {
      if (error) {
        resolve()
      }
      jsonData = JSON.parse(body)
      if (jsonData.error) resolve()

      resolve(jsonData);
    })
  })
}

// function to capoitalize first letter for gender : male or female
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

// format date after adding expiry date
function formatDate(date) {
  return ('{0}-{1}-{3} {4}:{5}:{6}').replace('{0}', date.getFullYear()).replace('{1}', date.getMonth() + 1).replace('{3}', date.getDate()).replace('{4}', date.getHours()).replace('{5}', date.getMinutes()).replace('{6}', date.getSeconds())
}


/**
 * API for delete user from our DB by userID
 */
async function _delete(id) {
  if (!id.match(/^[0-9a-fA-F]{24}$/)) {
    throw 'Please send the correct format for ID '
  }

  let user = await User.findById(id).select('-hash')

  if(!user || !user._id){
    throw "User Not Found"
  }

  await User.findByIdAndRemove(id);
}

// Function for get goals deatls according to user goals
async function getGoals(user) {
  if (user.goals && user.goals.length) {
    await Promise.all(
      user.goals.map(async item => {
        let goalID = mongoose.Types.ObjectId(item.id);
        let goal = await Goal.findOne({ _id: goalID })
        if (goal) {
          item.image = goal.image;
          item.title = goal.title;
        }
      })
    );
  }
  return user
}

/**
 * API for getUserByEmail and create token for that email
 */
async function getUserByEmail(email, baseURL) {
  email = email.toLowerCase()
  let user = await User.findOne({ email });

  if (user) {

    const token = jwt.sign({ sub: user.email }, config.secret);

    //setting new token
    user.resetToken = token;

    // save user with new token
    await user.save();
    let resetPwdLink = baseURL+"/reset?token="+token

    let dynamicLink = await getFirebaseDynamicLink(resetPwdLink)
    if(dynamicLink.shortLink){

      let firstName = user.firstName
      let lastName = user.lastName
      let userName = firstName+" "+lastName
      let resetPwdLink = dynamicLink.shortLink
      let subject = 'Reset your account password'
      let body = '<p>Click <a href="' + resetPwdLink + '">here</a> to reset your password</p>'

      let emailTemplate = await EmailTemplate.findOne({templateName:"forgotPassword"})
      if(emailTemplate && emailTemplate.subject && emailTemplate.subject != ""){
        emailTemplate.subject = emailTemplate.subject.replace(/{{ user_name }}/g, userName)
        emailTemplate.subject = emailTemplate.subject.replace(/{{ user_first_name }}/g, firstName)
        emailTemplate.subject = emailTemplate.subject.replace(/{{ user_last_name }}/g, lastName)

        subject = emailTemplate.subject
      }

      if(emailTemplate && emailTemplate.body && emailTemplate.body != ""){
        emailTemplate.body = emailTemplate.body.replace(/{{ user_name }}/g, userName)
        emailTemplate.body = emailTemplate.body.replace(/{{ user_first_name }}/g, firstName)
        emailTemplate.body = emailTemplate.body.replace(/{{ user_last_name }}/g, lastName)
        emailTemplate.body = emailTemplate.body.replace(/{{ reset_password_link }}/g, resetPwdLink)

        body = emailTemplate.body
      }

      await mailgu(email, subject, body) //send mail to the user vai mailgun

      return { status: "success", message: "Reset password email has been sent" }
    }else{
      throw "Error while reset password"
    }
  }
}

// Function to create dynamic link on firebase
async function getFirebaseDynamicLink(resetPwdLink){
  let fireBaseURL = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key='+config.firebase_apikey
  let method = "POST"
  let bodyParams = {
      "dynamicLinkInfo": {
        "domainUriPrefix": config.firebase_domainUriPrefix,
        "link": resetPwdLink,
        "androidInfo": {
          "androidPackageName": config.firebase_iosBundleId
        },
        "iosInfo": {
          "iosBundleId": config.firebase_iosBundleId
        }
      }
    }
    // Params for get dynamic link firebase
    var options = {
      'method': method,
      'url': fireBaseURL,
      'headers': {
        'Content-Type': 'application/json'
      },
      body:JSON.stringify(bodyParams)
    };

    //Send hit for create appointment in DRChrono
  return await sendRequestToFirebaseAsync(options).then(async jsonData => {
      if (jsonData && jsonData.shortLink) {
        return jsonData
      }else{
        return {}
      }
    })

}

// send hit to create dynsmic url from firebase
const sendRequestToFirebaseAsync = (options) => {

  return new Promise(resolve => {
    request(options, async function (error, response, body) {

      if (error || body == "" || body == " ") {
        resolve()
      } else {
        jsonData = JSON.parse(body)
        if (jsonData.error) {
          resolve()
        }

        resolve(jsonData);
      }
    })
  })
}

/**
 * API for getUserByPhone and create token for that email
 */
async function getUserByPhone(phone) {
  let user = await User.findOne({ phone });

  if (user) {
    const token = jwt.sign({ sub: user.email }, config.secret);

    //setting new token
    user.resetToken = token;

    // save user with new token
    await user.save();

    return { status: "success", message: "" , token:token}
  }
}

/**
 * API for set new password with refresh token when forgot password is clicked
 */
async function setNewPasswordWithToken(query, body) {
  if (!body.password) throw 'Enter password first'
  if (!body.confirmPassword) throw 'Enter confirm password'
  var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})")
  if (!passwordRegex.test(body.password)) {
    throw 'Password must be greater than or equals to 8 characters, Contains 1 uppercase, 1 lowercase and 1 number'
  }

  if (body.password !== body.confirmPassword) return { status: "error", message: "Password not matched." }

  let user = await User.findOne({ resetToken: query.token });

  if (user && user.email && query.token) {
    //hasing new password
    user.hash = bcrypt.hashSync(body.password, 10);
    user.resetToken = null;
    // save user with new password
    await user.save();

    return { status: "success", message: "Password updated successfully" }
  }
}

/**
 * API for check if refresh token is exits
 */
async function resetCheckTokenAndData(token) {

  let user = await User.findOne({ resetToken: token });

  if (user && token) {

    return user
  }
}

/**
 * API for check if the phone is exits or valid
 */
async function checkExistingPhoneNumber(phone) {
  // let user = await User.findOne({ phone })
  // if (!user) {
  //   return { status: "success", message: "Phone Number is valid" }
  // }
  return { status: "success", message: "Phone Number is valid" }
}

/**
 * API for verify phone number if OTP is valid. OTP handled by frontend
 */
async function verifyPhoneNumber(phone) {
  let user = await User.findOne({ phone })
  if (user) {
    user.isVerifiedPhone = true;
    await user.save();
    const token = jwt.sign({ sub: user.id }, config.secret);
    return { status: "success", message: "Phone number verified successfully", token: token }
  }
}

/**
 * API for verify email if email is valid or not
 */
async function verifyEmail(email) {
  let user = await User.findOne({ email });
  if (!user) return { status: "success", message: "Email is valid" }
}
