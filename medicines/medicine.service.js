﻿const db = require('_helpers/db');
var mongoose = require('mongoose');
const Medicine = db.Medicine;

module.exports = {
    getAll,
    createMedicines,
    updateMedicines,
    deleteMedicines
};

/**
 * API for get all medicines
 */
async function getAll() {
  return await Medicine.find().sort({$natural: -1})
}

/**
 * API for create medicines
 */
async function createMedicines(medicineParams, medicineImage) {

  if(medicineImage){
    medicineParams.image = medicineImage.replace('public/', '')
  }

  const medicine = new Medicine(medicineParams);
  medicine.save()

  return {status:'success', message:'Medicine Saved successfully'}
}

/**
 * API for update medicines
 */
async function updateMedicines(medicineParams, medicineID, medicineImage) {

  if(medicineImage){
    medicineParams.image = medicineImage.replace('public/', '')
  }

  const medicine = await Medicine.findById(medicineID)

  Object.assign(medicine, medicineParams);
  await medicine.save()

  return {status:'success', message:'Medicine updated successfully'}
}

/**
 * API for delete medicines
 */
async function deleteMedicines(medicineID) {
  console.log(medicineID, "medicineID")
  await Medicine.findByIdAndRemove(medicineID);

  return {status:"success", message:"Medicine removed"}
}
