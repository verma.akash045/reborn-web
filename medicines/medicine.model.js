const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: { type: String },
    salt: { type: String },
    brand: { type: String },
    description: { type: String },
    cost: { type: String },
    image: { type: String },
},{
  timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Medicine', schema);
