const express = require('express');
const router = express.Router();
const session = require('express-session')
const medicineService = require('./medicine.service');
const fs = require('fs');
var multer  =   require('multer');

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './public/uploads/medicines');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
  }
});

var upload = multer({ storage : storage}).single('medicine_image');

// routes
router.get('/', getAll);

// POST
router.post('/create', createMedicines);
router.post('/update', updateMedicines);

// DELETE
router.delete('/delete', deleteMedicines);

module.exports = router;

// Function for get all goals
function getAll(req, res, next) {
  if(req.session.token){
    medicineService.getAll()
        .then(medicines => medicines ? res.render('medicines', {page:'Medicines', medicines:medicines, error:""}) : res.render('medicines', {page:'Medicines', medicines:"Medicines not found", error:""} ) )
        .catch(err => res.render('medicines', {page:'Medicines', medicines:"", error:err}));
  }else{
    res.redirect('/adminLogin')
  }
}

// Function for delete medicine
function deleteMedicines(req, res, next) {
  medicineService.deleteMedicines(req.body.medicineID)
    .then(medicines => medicines ? res.json(medicines) : res.json([]) )
    .catch(err => next(err) );
}

// Function for create medicine
function createMedicines(req, res, next) {
  let dirpath = './public/uploads/medicines/'
  if(!fs.existsSync(dirpath)){
    fs.mkdirSync(dirpath, { recursive: true })
  }
  upload(req,res,function(err) {
        if(err) {
            console.log(err, "file_error")
            return res.end("Error uploading file.");
        }
        proPic = ''
        if(req.file && req.file.path){
          proPic = req.file.path
        }

        medicineService.createMedicines(req.body, proPic)
          .then(medicines => medicines ? res.json(medicines) : res.json([]) )
          .catch(err => next(err) );
    });

}

// Function for create medicine
function updateMedicines(req, res, next) {
  upload(req,res,function(err) {
        if(err) {
            console.log(err, "file_error")
            return res.end("Error uploading file.");
        }
        proPic = ''
        if(req.file && req.file.path){
          proPic = req.file.path
        }

        medicineService.updateMedicines(req.body, req.query.medicineID, proPic)
          .then(medicines => medicines ? res.json(medicines) : res.json([]) )
          .catch(err => next(err) );
    });

}
